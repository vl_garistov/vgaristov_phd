## Initial settings for NMOS power device simulations

File
{
	********** INPUT FILES **********
	* Geometry, contacts doping and mesh
	Grid = "@tdr@"
	* Physical parameters
	Parameter = "@parameter@"
	********** OUTPUT FILES **********
	* Distributed variables
	Plot = "@tdrdat@"
	* Electrical characteristics at the electrodes
	Current = "@plot@"
	* Log file
	Output = "@log@"
}

Electrode
{
	{
		Name = "drain_contact"
		Voltage = 0.0
	}
	{
		Name = "source_contact"
		Voltage = 0.0
	}
	{
		Name = "gate_contact"
		Voltage = 0.0
		#if "@tool_label@" == "sdevice_breakdown"
		Resist = @<Rcs*AF>@
		#elif "@tool_label@" == "sdevice_TLP"
		Current =
		(
			(0, 0.0e-16)
			(1e-9, @Itlp@)
			(11e-9, @Itlp@)
			(12e-9, 0.0e-16)
		)
		#endif
	}
	#if @GR@
	{
		Name = regexp("guard_rings_contact_[0-9]+")
		Voltage = 0.0
		Resist = @<1e8*AF>@
	}
	{
		Name = regexp("channel_stop_contact_[0-9]+")
		Voltage = 0.0
		Resist = @<1e8*AF>@
	}
	#endif
}

Thermode
{
	{
		Name = "drain_contact"
		Temperature = 300
		SurfaceResistance = 0.0015
	}
	{
		Name = "source_contact"
		Temperature = 300
		SurfaceResistance = 0.005
	}
}

Physics
{
	Fermi
	AreaFactor = @AF@
	Temperature = @Tamb@
	DefaultParametersFromFile
	EffectiveIntrinsicDensity
	(
		OldSlotboom
		NoFermi
	)
	Recombination
	(
		SRH
		(
			DopingDependence
			(
				Nakagawa
			)
			TempDependence
			ElectricField
			(
				Lifetime = Hurkx
				DensityCorrection = Local
			)
		)
		SurfaceSRH
		Auger
		Avalanche
		(
			Hatakeyama
			Eparallel
		)
	)
	Mobility
	(
		ConstantMobility
		PhuMob
		Enormal
		(
			IALMob
			InterfaceCharge
		)
		HighFieldSaturation
		(
			CaugheyThomas
		)
	)
	IncompleteIonization
	(
		Split
		(
			Doping = "NitrogenConcentration"
			Weights = (0.5 0.5)
		)
	)
	Aniso
	(
		Mobility
		Avalanche
		Poisson
		Temperature
		direction(SimulationSystem) = (1, 0, 0)
	)
	Thermodynamic
	TEPower
	(
		Analytic
	)
	HeatCapacity
	(
		TempDep
	)
	ThermalConductivity
	(
		TempDep
		Resistivity
	)
}

## Interface charges at SiliconCarbide-Oxide interface
Physics(MaterialInterface = "4HSiC/SiO2")
{
	Traps
	(
		FixedCharge Conc = @FC@
	)
}

Math
{
	NumberofThreads = 4

	Extrapolate
	(
		* Prevents convergence issues caused by incorrect extrapolation with very low carrier concentrations
		LowDensityLimit = 1e8
		NumberOfFailures = 2
	)
	Iterations = 20
	Notdamped = 30

	ExtendedPrecision(80)
	Digits = 7

	CheckRhsAfterUpdate
	RelErrControl
	RhsMin = 1e-7
	RHSMax = 1e60
	RHSFactor = 1e60
	* Typos?
	* ErrEffSlivovaRakia(electron) = 1e8
	* ErrEff(hole) = 1e8
	ErrRef(electron) = 1e8
	ErrRef(hole) = 1e8

	ExitOnFailure
	* BreakAtOBA
	* AvalPostProcessing
	* BreakCriteria{Current (Contact="drain_contact" absval=1.0)}

	CDensityMin = 1e-25
	AvalDensGradQF
	TensorGridAniso(aniso)

	* The default is actually more accurate but slower and less convergent
	* Transient = BE

	Method = Blocked
	SubMethod = ILS
	(
		set = 12
	)
	ILSrc = "
		set (12)
		{
			iterative(gmres(150), tolrel = 1e-9, tolunprec = 1e-4, tolabs = 0, maxit = 200);
			preconditioning(ilut(1e-8, -1), left);
			ordering(symmetric=nd, nonsymmetric = mpsilst);
			options(compact = yes, linscale = 0, refineresidual = 5, verbose = 0);
		};"
}

Plot
{
	Potential SpaceCharge
	eDensity hDensity
	eCurrent hCurrent TotalCurrent/vector CurrentPotential
	ElectricField/vector
	eQuasiFermi hQuasiFermi
	egradQuasiFermi hgradQuasiFermi
	SRH Auger
	AvalancheGeneration eAvalanche hAvalanche
	eMobility hMobility
	eMobilityAniso hMobilityAniso
	eMobilityAnisoFactor hMobilityAnisoFactor
	DielectricConstant DielectricConstantAniso
	Doping DonorConcentration DonorPlusConcentration AcceptorConcentration AccepMinusConcentration
	NitrogenConcentration NitrogenActiveConcentration
	NitrogenConcentration_split1 NitrogenActiveConcentration_split1 NitrogenPlusConcentration_split1
	NitrogenConcentration_split2 NitrogenActiveConcentration_split2 NitrogenPlusConcentration_split2
	AluminumConcentration AluminumActiveConcentration
	ConductionBandEnergy ValenceBandEnergy BandGap

	* Traps visualization
	eTrappedCharge hTrappedCharge
	eInterfaceTrappedCharge hInterfaceTrappedCharge
	eGapStatesRecombination hGapStatesRecombination
	TotalInterfaceTrapConcentration

	LatticeTemperature
}
