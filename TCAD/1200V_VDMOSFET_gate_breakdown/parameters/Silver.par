* Copyright (c) 1994-2023 Synopsys, Inc.
* This parameter file and the associated documentation are proprietary
* to Synopsys, Inc.  This parameter file may only be used in accordance
* with the terms and conditions of a written license agreement with
* Synopsys, Inc.  All other use, reproduction, or distribution of this
* parameter file is strictly prohibited.


Epsilon
{ *  Ratio of the permittivities of material and vacuum

  * epsilon() = epsilon
	epsilon	= 0.0000e+00	# [1]
}


Epsilon_Inf
{ *  Ratio of the high frequency limit of permittivities of material and vacuum

  * epsilon_inf() = epsilon_inf
	epsilon_inf	= 0.0000e+00	# [1]
}


Epsilon_aniso
{ *  Ratio of the permittivities of material and vacuum

  * epsilon() = epsilon
	epsilon	= 0.0000e+00	# [1]
}


Epsilon_Inf_aniso
{ *  Ratio of the high frequency limit of permittivities of material and vacuum

  * epsilon_inf() = epsilon_inf
	epsilon_inf	= 0.0000e+00	# [1]
}


RefractiveIndex
{ *  Optical Refractive Index

  * refractiveindex() = refractiveindex * (1 + alpha * (T-Tpar))
	Tpar	= 3.0000e+02	# [K]
	refractiveindex	= 0.0000e+00	# [1]
	alpha	= 2.0000e-04	# [1/K]

  * Gain dependence of refractive index in active region:
  * a) Linear model: delta n = a0 * ( (n+p)/(2 * N0) - 1)
  * b) Logarithmic model: delta n = a0 * log ( (n+p)/(2 * N0) )
  * where n/p are the carrier densities in the active region. 
	a0	= 0.0000e+00	# [1]
	N0	= 1.0000e+18	# [1/cm^3]
}


ComplexRefractiveIndex
{ *  Complex refractive index model: n_complex = n + i*k (unitless)
  *  
  *  with n = n_0 + delta_n_lambda + delta_n_T + delta_n_carr + delta_n_gain 
  *       k = k_0 + delta_k_lambda             + delta_k_carr                
  
  * Base refractive index and extinction coefficient: 
  *     n_0, k_0 
  
  * Wavelength dependence (real and imag): 
  *     Formula 0: delta_n_lambda = Cn_lambda * lambda + Dn_lambda * lambda^2 
  *                delta_k_lambda = Ck_lambda * lambda + Dk_lambda * lambda^2 
  *     Formula 1: Read tabulated values 
  *                NumericalTable (...)  
  *     Formula 2: Read tabulated values from file 
  *                NumericalTable = <string> 
  
  * Temperature dependence (real): 
  *     delta_n_T = n_0 * ( Cn_temp * (T-Tpar)) 
  
  * Carrier dependence (real) 
  *     delta_n_carr = - Cn_carr * (const.) * (n/m_e + p/m_h) 
  
  * Carrier dependence (imag) 
  *     delta_k_carr = 1 / (4*PI) * (wavelength^Gamma_k_carr_e*Ck_carr_e*n + wavelength^Gamma_k_carr_h*Ck_carr_h*p) 
  
  * Gain dependence (real) 
  *     lin: delta_n_gain = Cn_gain * ( (n+p)/(2 * Npar) - 1) 
  *     log: delta_n_gain = Cn_gain * log ( (n+p)/(2 * Npar ) )
	n_0	= 1	# [1]
	k_0	= 0.0000e+00	# [1]
	Cn_lambda	= 0.0000e+00	# [um^-1]
	Dn_lambda	= 0.0000e+00	# [um^-2]
	Ck_lambda	= 0.0000e+00	# [um^-1]
	Dk_lambda	= 0.0000e+00	# [um^-2]
	Cn_temp	= 2.0000e-04	# [K^-1]
	Cn_carr	= 1	# [1]
	Ck_carr	= 0.0000e+00 ,	0.0000e+00	# [cm^2]
	Gamma_k_carr	= 1 ,	1	# [1]
	Cn_gain	= 0.0000e+00	# [1]
	Npar	= 1.0000e+18	# [cm^-3]
        Formula = 1
        TableInterpolation = PositiveSpline, PositiveSpline
        NumericalTable ( 
          0.0506	0.906	0.522;
          0.0539	0.98	0.614;
          0.0551	1.029	0.624;
          0.0576	1.104	0.584;
          0.059	1.109	0.561;
          0.062	1.098	0.548;
          0.0653	1.096	0.565;
          0.0729	1.121	0.635;
          0.0775	1.167	0.676;
          0.08	1.202	0.691;
          0.0855	1.281	0.679;
          0.0918	1.322	0.628;
          0.0992	1.315	0.588;
          0.1025	1.308	0.581;
          0.1051	1.304	0.578;
          0.1107	1.293	0.566;
          0.1137	1.28	0.56;
          0.117	1.265	0.56;
          0.1204	1.252	0.564;
          0.124	1.241	0.568;
          0.1278	1.229	0.566;
          0.1348	1.182	0.55;
          0.1378	1.149	0.552;
          0.1409	1.112	0.563;
          0.1442	1.073	0.581;
          0.1476	1.032	0.61;
          0.1512	0.993	0.653;
          0.155	0.962	0.706;
          0.159	0.94	0.77;
          0.1631	0.935	0.832;
          0.1675	0.936	0.892;
          0.1722	0.942	0.951;
          0.1771	0.953	1.01;
          0.1823	0.969	1.07;
          0.1879	0.995	1.13;
          0.1937	1.028	1.18;
          0.1968	1.048	1.21;
          0.2	1.072	1.24;
          0.2033	1.098	1.26;
          0.2066	1.125	1.27;
          0.2138	1.173	1.29;
          0.2214	1.208	1.3;
          0.2296	1.238	1.31;
          0.2384	1.265	1.33;
          0.248	1.298	1.35;
          0.253	1.32	1.35;
          0.2583	1.343	1.35;
          0.2638	1.372	1.35;
          0.2695	1.404	1.33;
          0.2755	1.441	1.31;
          0.2818	1.476	1.26;
          0.2883	1.502	1.19;
          0.2952	1.519	1.08;
          0.2988	1.522	0.992;
          0.3024	1.496	0.882;
          0.3061	1.432	0.766;
          0.31	1.323	0.647;
          0.3115	1.246	0.586;
          0.3139	1.149	0.54;
          0.3155	1.044	0.514;
          0.3179	0.932	0.504;
          0.3195	0.815	0.526;
          0.322	0.708	0.565;
          0.3237	0.616	0.609;
          0.3263	0.526	0.663;
          0.3306	0.371	0.813;
          0.3324	0.321	0.902;
          0.3351	0.294	0.986;
          0.3397	0.259	1.12;
          0.3444	0.238	1.24;
          0.3542	0.209	1.44;
          0.3647	0.2	1.61;
          0.3757	0.198	1.67;
          0.3875	0.192	1.81;
          0.4	0.173	1.95;
          0.4133	0.173	2.11;
          0.4275	0.16	2.26;
          0.4428	0.157	2.4;
          0.4592	0.144	2.56;
          0.4769	0.132	2.72;
          0.4959	0.13	2.88;
          0.5166	0.13	3.07;
          0.5391	0.129	3.25;
          0.5636	0.12	3.45;
          0.5904	0.121	3.66;
          0.6199	0.131	3.88;
          0.6526	0.14	4.15;
          0.6888	0.14	4.44;
          0.7293	0.148	4.74;
          0.7749	0.143	5.09;
          0.8266	0.145	5.5;
          0.8856	0.163	5.95;
          0.9537	0.198	6.43;
          1.033	0.226	6.99;
          1.127	0.251	7.67;
          1.24	0.329	8.49;
          1.265	0.37	7.78;
          1.291	0.38	7.92;
          1.305	0.38	7.95;
          1.319	0.392	8.06;
          1.348	0.401	8.21;
          1.378	0.411	8.37;
          1.409	0.421	8.37;
          1.459	0.44	8.8;
          1.512	0.455	9.08;
          1.55	0.469	9.32;
          1.59	0.485	9.57;
          1.631	0.501	9.84;
          1.675	0.519	10.1;
          1.722	0.537	10.4;
          1.771	0.557	10.7;
          1.823	0.578	11.1;
          1.879	0.6	11.4;
          1.937	0.624	11.8;
          2	0.65	12.2;
          2.066	0.668	12.6;
          2.138	0.729	13;
          2.214	0.774	13.5;
          2.296	0.823	14;
          2.384	0.878	14.5;
          2.48	0.939	15.1;
          2.583	1.007	15.7;
          2.695	1.083	16.4;
          2.818	1.168	17.1;
          2.952	1.265	17.9;
          3.1	1.387	18.8;
          3.263	1.536	19.8;
          3.444	1.71	20.9;
          3.647	1.915	22.1;
          3.875	2.16	23.5;
          4.133	2.446	25.1;
          4.428	2.786	26.9;
          4.769	3.202	29;
          5.166	3.732	31.3;
          5.636	4.425	34;
          6.199	5.355	37;
          6.526	5.96	38.6;
          6.888	6.67	40.4;
          7.293	7.461	42.5;
          7.749	8.376	44.8;
          8.266	9.441	47.1;
          8.856	10.69	49.4;
          9.537	12.21	52.2;
          9.919	13.11	53.7;
          10	13.2	54;
          10.001	13.2	54;
          10.002	13.2	54;
        ) 
	Tpar	= 3.0000e+02	# [K]
} 


* SpectralConversion
* { * Spectral Conversion Model
*   No default model, user has to define.
*   All wavelength parameters should be in nanometers.
*   Choice of Analytic or NumericalTable selected in Physics section of region
*  
*   ConversionEfficiency = float     * ratio of absorbed photons that are reemitted.
*   AbsorptionScaling = float        * scale absorption
*   EmissionScaling = float          * scale emission
*   Analytic (
*     AbsorptionProfile = (
*        Gaussian(lambda0 sigma peakvalue dc_offset lambda_range0 lambda_range1)
*        Lorentzian(lambda0 width peakvalue dc_offset lambda_range0 lambda_range1)
*        ...
*     )
*     EmissionProfile = (
*        Gaussian(lambda0 sigma peakvalue dc_offset lambda_range0 lambda_range1)
*        Lorentzian(lambda0 width peakvalue dc_offset lambda_range0 lambda_range1)
*        ...
*     )
*   )
*   NumericalTable (
*     AbsorptionProfile = (
*        lambda0 value0
*        lambda1 value1
*        ...
*     )
*     EmissionProfile = (
*        lambda0 value0
*        lambda1 value1
*        ...
*     )

*   ConversionEfficiency = 1.0
* }


LatticeHeatCapacity
{ *  lumped electron-hole-lattice heat capacity

  * cv() = cv + cv_b * T + cv_c * T^2 + cv_d * T^3 
	cv	= 2.44	# [J/(K cm^3)]
	cv_b	= 0.0000e+00	# [J/(K^2 cm^3)]
	cv_c	= 0.0000e+00	# [J/(K^3 cm^3)]
	cv_d	= 0.0000e+00	# [J/(K^4 cm^3)]
}


Kappa
{ *  Lattice thermal conductivity

  * Formula = 0:
  * kappa() = 1 / ( 1/kappa + 1/kappa_b * T + 1/kappa_c * T^2 )

  * Formula = 1:
  * kappa() = kappa + kappa_b * T + kappa_c * T^2 

  Formula = 1
	kappa	= 4.18	# [W/(K cm)]
	kappa_b	= 0.0000e+00	# [W/(K^2 cm)]
	kappa_c	= 0.0000e+00	# [W/(K^3 cm)]


  * AllDependent Thermal Conductivity
	wL_n	= 0.2	# [1]
	wL_p	= 0.02	# [1]
	wT_n	= 8	# [1]
	wT_p	= 6	# [1]
	wTU_n	= 9.0000e+05	# [1]
	wTU_p	= 2.0000e+06	# [1]
	b	= 1.8600e+02	# [1]
	M_n	= 30.9	# [Da]
	M_p	= 10.8	# [Da]
	R_n	= 1.2300e-10	# [m]
	R_p	= 1.1700e-10	# [m]
	r_del	= 1	# [1]
	Ax	= 0.0000e+00	# [s^3]
	eta	= 1.5000e-10	# [m]
	Eu_n	= 9	# [eV]
	Eu_p	= 45	# [eV]
	ED_n	= 1.33	# [eV]
	ED_p	= 1.33	# [eV]
	dLy	= 0.3	# [m]
	Lb	= 7.1600e-03	# [m]
	Q_n	= 4	# [1]
	Q_p	= 15	# [1]
	Nomg	= 3	# [1]
	order	= 2	# [1]

      ** NumericalTable (
      **   Define table for parameters with format below
      **   N ED_n ED_p b Ax
      **   If this is defined, parameters ED_n/ED_p/b/Ax defined above are ignored
      ** ) 

	epsilon	= 11.7	# [F/cm]
	m_c	= 0.9	# [kg]
	m_v	= 0.58	# [kg]
	rho	= 2.3290e+03	# [kg/m^3]
	BL	= 2.0000e-24	# [s/K^3]
	BT	= 9.3000e-13	# [1/K^4]
	BTU	= 5.5000e-18	# [s]
	omega1	= 2.3570e+13	# [1/s]
	omega2	= 2.7490e+13	# [1/s]
	omega3	= 7.4630e+13	# [1/s]
	omega4	= 4.5820e+13	# [1/s]
	M_h	= 28	# [Da]
	R_h	= 1.4600e-10	# [m]
	V_h	= 1.2100e-05	# [m^3/mol]
	vL	= 8.4800e+03	# [m/s]
	vLp	= 4.2400e+03	# [m/s]
	vT	= 5.8600e+03	# [m/s]
	vTU	= 2.0000e+03	# [m/s]
	AI	= 1.3200e-45	# [s^3]
}


Kappa_aniso
{ *  Lattice thermal conductivity

  * Formula = 0:
  * kappa() = 1 / ( 1/kappa + 1/kappa_b * T + 1/kappa_c * T^2 )

  * Formula = 1:
  * kappa() = kappa + kappa_b * T + kappa_c * T^2 

  Formula = 1
	kappa	= 4.18	# [W/(K cm)]
	kappa_b	= 0.0000e+00	# [W/(K^2 cm)]
	kappa_c	= 0.0000e+00	# [W/(K^3 cm)]


  * AllDependent Thermal Conductivity
	wL_n	= 0.2	# [1]
	wL_p	= 0.02	# [1]
	wT_n	= 8	# [1]
	wT_p	= 6	# [1]
	wTU_n	= 9.0000e+05	# [1]
	wTU_p	= 2.0000e+06	# [1]
	b	= 1.8600e+02	# [1]
	M_n	= 30.9	# [Da]
	M_p	= 10.8	# [Da]
	R_n	= 1.2300e-10	# [m]
	R_p	= 1.1700e-10	# [m]
	r_del	= 1	# [1]
	Ax	= 0.0000e+00	# [s^3]
	eta	= 1.5000e-10	# [m]
	Eu_n	= 9	# [eV]
	Eu_p	= 45	# [eV]
	ED_n	= 1.33	# [eV]
	ED_p	= 1.33	# [eV]
	dLy	= 0.3	# [m]
	Lb	= 7.1600e-03	# [m]
	Q_n	= 4	# [1]
	Q_p	= 15	# [1]
	Nomg	= 3	# [1]
	order	= 2	# [1]

      ** NumericalTable (
      **   Define table for parameters with format below
      **   N ED_n ED_p b Ax
      **   If this is defined, parameters ED_n/ED_p/b/Ax defined above are ignored
      ** ) 

	epsilon	= 11.7	# [F/cm]
	m_c	= 0.9	# [kg]
	m_v	= 0.58	# [kg]
	rho	= 2.3290e+03	# [kg/m^3]
	BL	= 2.0000e-24	# [s/K^3]
	BT	= 9.3000e-13	# [1/K^4]
	BTU	= 5.5000e-18	# [s]
	omega1	= 2.3570e+13	# [1/s]
	omega2	= 2.7490e+13	# [1/s]
	omega3	= 7.4630e+13	# [1/s]
	omega4	= 4.5820e+13	# [1/s]
	M_h	= 28	# [Da]
	R_h	= 1.4600e-10	# [m]
	V_h	= 1.2100e-05	# [m^3/mol]
	vL	= 8.4800e+03	# [m/s]
	vLp	= 4.2400e+03	# [m/s]
	vT	= 5.8600e+03	# [m/s]
	vTU	= 2.0000e+03	# [m/s]
	AI	= 1.3200e-45	# [s^3]
}

Bandgap
{ * For conductors Band Gap is zero and the following parameters are used:
	WorkFunction	= 5.1	# [eV]
	FermiEnergy	= 11.7	# [eV]
  * for backward compatibility Chi0 could be used to define the work function.
}

Resistivity
{ * Resist(T) = Resist0 * ( 1 + TempCoef * ( T - 273 ) + TempCoef2 * ( T - 273 )^2 )
	Resist0	= 1.5100e-06	# [ohm*cm]
	TempCoef	= 4.1000e-03	# [1/K]
	TempCoef2	= 0.0000e+00	# [1/K^2]
}

Resistivity_aniso
{ * Resist(T) = Resist0 * ( 1 + TempCoef * ( T - 273 ) + TempCoef2 * ( T - 273 )^2 )
	Resist0	= 1.5100e-06	# [ohm*cm]
	TempCoef	= 4.1000e-03	# [1/K]
	TempCoef2	= 0.0000e+00	# [1/K^2]
}

