
****************************************************************************
***                       Sentaurus Workbench gsub                       ***
***                          Version V-2023.12                           ***
***                      (2.8515123, x86_64, Linux)                      ***
***                                                                      ***
***                Copyright (c) 1994-2023 Synopsys, Inc.                ***
***                                                                      ***
***  This software and the associated documentation are confidential     ***
***  and proprietary to Synopsys, Inc.  Your use or disclosure of this   ***
***  software is subject to the terms and conditions of a written        ***
***  license agreement between you, or your company, and Synopsys, Inc.  ***
****************************************************************************

gsub is running on host 'l2s1.localdomain' by user 'v_garistov'
Binary '/eda/synopsys/2023-24/RHELx86/SENTAURUS_2023.12/tcad/current/linux64/bin/../lib/gsub0'
Command line options '-verbose -swb_pid 513449 -q local:default -e remaining /home/v_garistov/STDB/1200V_VDMOSFET'
Current directory '/home/v_garistov/STDB'
Project organization 'Hierarchical'
Some relevant environment variables:
	STROOT=/eda/synopsys/2023-24/RHELx86/SENTAURUS_2023.12
	STRELEASE=current
	STROOT_LIB=/eda/synopsys/2023-24/RHELx86/SENTAURUS_2023.12/tcad/current/lib
	STDB=/home/v_garistov/STDB
	DATEX=/eda/synopsys/2023-24/RHELx86/SENTAURUS_2023.12/tcad/current/lib/datexcodes.txt
	DISPLAY=10.14.0.10:10.0

Starting swblm daemon...OK
Project '/home/v_garistov/STDB/1200V_VDMOSFET'
Application mode : hierarchical
Loading project variables '/home/v_garistov/STDB/1200V_VDMOSFET/gvars.dat'

+++ Evaluating prologue Tcl script...

23:13:43 Jun 11 2024 <1200V_VDMOSFET> submitted to the batch system
Loading global queue configuration file '/eda/synopsys/2023-24/RHELx86/SENTAURUS_2023.12/queues/gqueues.dat'...ok
Loading project tree '/home/v_garistov/STDB/1200V_VDMOSFET/gtree.dat'
>>>>> Preprocessing the project to run in normal mode
Loading jobs and dependence graph '"/home/v_garistov/STDB/1200V_VDMOSFET/gexec.cmd"'
INITIALIZING PREPROCESSOR:
Loading project tree '/home/v_garistov/STDB/1200V_VDMOSFET/gtree.dat'
Loading project variables '/home/v_garistov/STDB/1200V_VDMOSFET/gvars.dat'
--Get files to preprocess...
--Create file handlers...
--Read the toolflow...
--Analyze tool-file dependencies...
--Read all nodes to preprocess...
--Group nodes into experiments...
--Get all variables...
--Get all parameters...
PREPROCESSOR SUCCESSFULLY INITIALIZED.
PREPROCESSING STEP 1:
Reading file /home/v_garistov/STDB/1200V_VDMOSFET/sde_dvs.cmd
Reading file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice_output_des.cmd
Include the contents of the  file 'init_vdmos_des.cmd' in the file '/home/v_garistov/STDB/1200V_VDMOSFET/sdevice_output_des.cmd

Reading file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice.par
Reading file /home/v_garistov/STDB/1200V_VDMOSFET/svisualpy_output_vis.py
Reading file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice_transfer_des.cmd
Include the contents of the  file 'init_vdmos_des.cmd' in the file '/home/v_garistov/STDB/1200V_VDMOSFET/sdevice_transfer_des.cmd

Reading file /home/v_garistov/STDB/1200V_VDMOSFET/svisualpy_transfer_vis.py
PREPROCESSING STEP 2:
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice_output_des.cmd for node 8
'@tdr@' creates dependences with the node(s) '4'
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice.par for node 8
+++Node successfully preprocessed+++
The node '8' has dependencies to the node(s) '4'
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice_output_des.cmd for node 10
'@tdr@' creates dependences with the node(s) '4'
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice.par for node 10
+++Node successfully preprocessed+++
The node '10' has dependencies to the node(s) '4'
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice_output_des.cmd for node 11
'@tdr@' creates dependences with the node(s) '4'
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice.par for node 11
+++Node successfully preprocessed+++
The node '11' has dependencies to the node(s) '4'
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/svisualpy_output_vis.py for node 13
'@previous@' creates dependences with the node(s) '8'
+++Node successfully preprocessed+++
The node '13' has dependencies to the node(s) '8'
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/svisualpy_output_vis.py for node 15
'@previous@' creates dependences with the node(s) '10'
+++Node successfully preprocessed+++
The node '15' has dependencies to the node(s) '10'
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/svisualpy_output_vis.py for node 16
'@previous@' creates dependences with the node(s) '11'
+++Node successfully preprocessed+++
The node '16' has dependencies to the node(s) '11'
Writing 'gexec.cmd'
Loading jobs and dependence graph '"/home/v_garistov/STDB/1200V_VDMOSFET/gexec.cmd"'
Warning: node '1' not queued: node is virtual
Warning: node '2' not queued: node is virtual
Warning: node '3' not queued: node is virtual
Warning: node '5' not queued: node is virtual
Warning: node '6' not queued: node is virtual
>>>>>>>>>> job '8' status changed from 'aborted' to 'queued'
>>>>>>>>>> job '10' status changed from 'aborted' to 'queued'
>>>>>>>>>> job '11' status changed from 'aborted' to 'queued'
>>>>>>>>>> job '13' status changed from 'none' to 'queued'
>>>>>>>>>> job '15' status changed from 'none' to 'queued'
>>>>>>>>>> job '16' status changed from 'none' to 'queued'
Warning: node '17' not queued: node is virtual
Warning: node '18' not queued: node is virtual
Warning: node '19' not queued: node is virtual
Warning: node '20' not queued: node is virtual
Warning: node '21' not queued: node is virtual
Warning: node '22' not queued: node is virtual
Warning: node '23' not queued: node is virtual
Warning: node '24' not queued: node is virtual
Warning: node '25' not queued: node is virtual
Warning: node '26' not queued: node is virtual
-----------------------------------------------------------------
queue "local:default" - 8 10 11 13 15 16
-----------------------------------------------------------------
>>>>>>>>>> Initializing back-end schedulers...
>>>>>>>> Loaded Schedulers are ... local
local% initialize 'local' scheduler data
>>>>> List of Active Schedulers : local
<<<<<<<<<<
>>>>>>>>>> job '8' status changed from 'queued' to 'ready'
INITIALIZING PREPROCESSOR:
Loading project tree '/home/v_garistov/STDB/1200V_VDMOSFET/gtree.dat'
Loading jobs and dependence graph '"/home/v_garistov/STDB/1200V_VDMOSFET/gexec.cmd"'
Loading project variables '/home/v_garistov/STDB/1200V_VDMOSFET/gvars.dat'
--Get files to preprocess...
--Create file handlers...
--Read the toolflow...
--Analyze tool-file dependencies...
--Read all nodes to preprocess...
--Group nodes into experiments...
--Get all variables...
--Get all parameters...
PREPROCESSOR SUCCESSFULLY INITIALIZED.
PREPROCESSING STEP 1:
Reading file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice_output_des.cmd
Include the contents of the  file 'init_vdmos_des.cmd' in the file '/home/v_garistov/STDB/1200V_VDMOSFET/sdevice_output_des.cmd

Reading file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice.par
PREPROCESSING STEP 2:
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice_output_des.cmd for node 8
'@tdr@' creates dependences with the node(s) '4'
+++Preprocessing results written to /home/v_garistov/STDB/1200V_VDMOSFET/results/nodes/8/pp8_des.cmd
The node '8' has dependencies to the node(s) '4'
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice.par for node 8
+++Preprocessing results written to /home/v_garistov/STDB/1200V_VDMOSFET/results/nodes/8/pp8_des.par
The node '8' has dependencies to the node(s) '4'
>>>>>>>>>> os_rm /home/v_garistov/STDB/1200V_VDMOSFET/results/nodes/8/n8_des.job
local% submit job 8 for local execution
>>>>>>>>>> job '8' status changed from 'ready' to 'pending'
>>>>>>>>>> job '10' status changed from 'queued' to 'ready'
INITIALIZING PREPROCESSOR:
Loading project tree '/home/v_garistov/STDB/1200V_VDMOSFET/gtree.dat'
Loading jobs and dependence graph '"/home/v_garistov/STDB/1200V_VDMOSFET/gexec.cmd"'
Loading project variables '/home/v_garistov/STDB/1200V_VDMOSFET/gvars.dat'
--Get files to preprocess...
--Create file handlers...
--Read the toolflow...
--Analyze tool-file dependencies...
--Read all nodes to preprocess...
--Group nodes into experiments...
--Get all variables...
--Get all parameters...
PREPROCESSOR SUCCESSFULLY INITIALIZED.
PREPROCESSING STEP 1:
Reading file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice_output_des.cmd
Include the contents of the  file 'init_vdmos_des.cmd' in the file '/home/v_garistov/STDB/1200V_VDMOSFET/sdevice_output_des.cmd

Reading file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice.par
PREPROCESSING STEP 2:
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice_output_des.cmd for node 10
'@tdr@' creates dependences with the node(s) '4'
+++Preprocessing results written to /home/v_garistov/STDB/1200V_VDMOSFET/results/nodes/10/pp10_des.cmd
The node '10' has dependencies to the node(s) '4'
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice.par for node 10
+++Preprocessing results written to /home/v_garistov/STDB/1200V_VDMOSFET/results/nodes/10/pp10_des.par
The node '10' has dependencies to the node(s) '4'
>>>>>>>>>> os_rm /home/v_garistov/STDB/1200V_VDMOSFET/results/nodes/10/n10_des.job
local% submit job 10 for local execution
>>>>>>>>>> job '10' status changed from 'ready' to 'pending'
>>>>>>>>>> job '11' status changed from 'queued' to 'ready'
INITIALIZING PREPROCESSOR:
Loading project tree '/home/v_garistov/STDB/1200V_VDMOSFET/gtree.dat'
Loading jobs and dependence graph '"/home/v_garistov/STDB/1200V_VDMOSFET/gexec.cmd"'
Loading project variables '/home/v_garistov/STDB/1200V_VDMOSFET/gvars.dat'
--Get files to preprocess...
--Create file handlers...
--Read the toolflow...
--Analyze tool-file dependencies...
--Read all nodes to preprocess...
--Group nodes into experiments...
--Get all variables...
--Get all parameters...
PREPROCESSOR SUCCESSFULLY INITIALIZED.
PREPROCESSING STEP 1:
Reading file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice_output_des.cmd
Include the contents of the  file 'init_vdmos_des.cmd' in the file '/home/v_garistov/STDB/1200V_VDMOSFET/sdevice_output_des.cmd

Reading file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice.par
PREPROCESSING STEP 2:
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice_output_des.cmd for node 11
'@tdr@' creates dependences with the node(s) '4'
+++Preprocessing results written to /home/v_garistov/STDB/1200V_VDMOSFET/results/nodes/11/pp11_des.cmd
The node '11' has dependencies to the node(s) '4'
---------------------------------------
Preprocessing file /home/v_garistov/STDB/1200V_VDMOSFET/sdevice.par for node 11
+++Preprocessing results written to /home/v_garistov/STDB/1200V_VDMOSFET/results/nodes/11/pp11_des.par
The node '11' has dependencies to the node(s) '4'
>>>>>>>>>> os_rm /home/v_garistov/STDB/1200V_VDMOSFET/results/nodes/11/n11_des.job
local% submit job 11 for local execution
>>>>>>>>>> job '11' status changed from 'ready' to 'pending'
local% exec /eda/synopsys/2023-24/RHELx86/SENTAURUS_2023.12/bin/gjob -verbose -nice 19 --max_threads 4  -job 8 -swb_pid 513449 -gsub_pid 513621 -gsub_host l2s1.localdomain -gsub_port 36515  "/home/v_garistov/STDB/1200V_VDMOSFET"
local% exec /eda/synopsys/2023-24/RHELx86/SENTAURUS_2023.12/bin/gjob -verbose -nice 19 --max_threads 4  -job 10 -swb_pid 513449 -gsub_pid 513621 -gsub_host l2s1.localdomain -gsub_port 36515  "/home/v_garistov/STDB/1200V_VDMOSFET"
local% exec /eda/synopsys/2023-24/RHELx86/SENTAURUS_2023.12/bin/gjob -verbose -nice 19 --max_threads 4  -job 11 -swb_pid 513449 -gsub_pid 513621 -gsub_host l2s1.localdomain -gsub_port 36515  "/home/v_garistov/STDB/1200V_VDMOSFET"
>>>>>>>>>> job '8' status changed from 'pending' to 'running'
23:13:46 Jun 11 2024     job 8 <sdevice> started on host 'l2s1.localdomain': "sdevice pp8_des.cmd"
>>>>>>>>>> job '10' status changed from 'pending' to 'running'
23:13:46 Jun 11 2024     job 10 <sdevice> started on host 'l2s1.localdomain': "sdevice pp10_des.cmd"
>>>>>>>>>> job '11' status changed from 'pending' to 'running'
23:13:46 Jun 11 2024     job 11 <sdevice> started on host 'l2s1.localdomain': "sdevice --max_threads 4  pp11_des.cmd"
% aborting <1200V_VDMOSFET> ...
>>>>>>>>>> job '13' status changed from 'queued' to 'none'
>>>>>>>>>> job '15' status changed from 'queued' to 'none'
>>>>>>>>>> job '16' status changed from 'queued' to 'none'
local% kill jobs (SIGTERM): 513816 513821 513837
>>>>>>>>>> job '8' status changed from 'running' to 'aborted'
>>>>>>>>>> job '10' status changed from 'running' to 'aborted'
>>>>>>>>>> job '11' status changed from 'running' to 'aborted'

SCHEDULING REPORT

23:14:29 Jun 11 2024 <1200V_VDMOSFET> aborted: aborted
>>>>>> Writing to status file 46
Loading project variables '/home/v_garistov/STDB/1200V_VDMOSFET/gvars.dat'

+++ Evaluating epilogue Tcl script...

