# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "init_vdmos_des.cmd"

#define _ramp-up_	10
#define _hold_		10
#define _ramp-down_	10

Solve
{
	* Initial Solution
	Coupled
	(
		Iterations = 10000
		LineSearchDamping = 1e-3
	)
	{
		Poisson
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-3
	)
	{
		Poisson
		Electron
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-3
	)
	{
		Poisson
		Electron
		Hole
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-3
	)
	{
		Poisson
		Electron
		Hole
		Temperature
	}

	NewCurrentPrefix = "TLP_"
	Transient
	(
		InitialTime = 0.0
		FinalTime = 12e-9
		InitialStep = 1e-14
		MinStep = 1e-20
		MaxStep = 0.1
		Increment = 1.45
		Decrement = 3.0
		Plot
		{
			Range = (0, 1e-9)
			Intervals = _ramp-up_
		}
		Plot
		{
			Range = (1e-9, 11e-9)
			Intervals = _hold_
		}
		Plot
		{
			Range = (11e-9, 12e-9)
			Intervals = _ramp-down_
		}
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
			Temperature
		}
	}
}
