# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from enum import Enum

class Param(Enum):
	Rcs = 1
	Tox = 2
	Ng = 3
	Tg = 4
	Npwb = 5
	Npwt = 6
	Tpwb = 7
	Tpwt = 8
	L = 9
	Npp = 10
	Tpp = 11
	Lpp = 12
	Lsg = 13
	Nnp = 14
	Tnp = 15
	Lnp = 16
	Njfet = 17
	Tjfet = 18
	Ljfet = 19
	Nepi = 20
	Tepi = 21
	Nsub = 22
	Tsub = 23
	FC = 24
	GR = 25
	NF = 26

param_order = [
	Param.Rcs,
]

def get_param_long_name(param):
	match param:
		case Param.Rcs:
			return "series resistance"
		case Param.Tox:
			return "gate oxide thickness"
		case Param.Ng:
			return "gate doping"
		case Param.Tg:
			return "gate thickness"
		case Param.Npwb:
			return "burried P-well doping"
		case Param.Npwt:
			return "top P-well doping"
		case Param.Tpwb:
			return "burried P-well thickness"
		case Param.Tpwt:
			return "top P-well thickness"
		case Param.L:
			return "channel length"
		case Param.Npp:
			return "P+ doping"
		case Param.Tpp:
			return "P+ thickness"
		case Param.Lpp:
			return "P+ length"
		case Param.Lsg:
			return "source-gate overlap"
		case Param.Nnp:
			return "N+ doping"
		case Param.Tnp:
			return "N+ thickness"
		case Param.Lnp:
			return "N+ length"
		case Param.Njfet:
			return "JFET doping"
		case Param.Tjfet:
			return "JFET thickness"
		case Param.Ljfet:
			return "JFET length"
		case Param.Nepi:
			return "epitaxial doping"
		case Param.Tepi:
			return "epitaxial thickness"
		case Param.Nsub:
			return "substrate doping"
		case Param.Tsub:
			return "substrate thickness"
		case Param.FC:
			return "trapped charge density"
		case Param.GR:
			return "edge termination"
		case Param.NF:
			return "number of cells"

def get_param_short_name(param):
	match param:
		case Param.Rcs:
			return "Rcs"
		case Param.Tox:
			return "Tox"
		case Param.Ng:
			return "Ng"
		case Param.Tg:
			return "Tg"
		case Param.Npwb:
			return "Npwb"
		case Param.Npwt:
			return "Npwt"
		case Param.Tpwb:
			return "Tpwb"
		case Param.Tpwt:
			return "Tpwt"
		case Param.L:
			return "L"
		case Param.Npp:
			return "Npp"
		case Param.Tpp:
			return "Tpp"
		case Param.Lpp:
			return "Lpp"
		case Param.Lsg:
			return "Lsg"
		case Param.Nnp:
			return "Nnp"
		case Param.Tnp:
			return "Tnp"
		case Param.Lnp:
			return "Lnp"
		case Param.Njfet:
			return "Njfet"
		case Param.Tjfet:
			return "Tjfet"
		case Param.Ljfet:
			return "Ljfet"
		case Param.Nepi:
			return "Nepi"
		case Param.Tepi:
			return "Tepi"
		case Param.Nsub:
			return "Nsub"
		case Param.Tsub:
			return "Tsub"
		case Param.FC:
			return "FC"
		case Param.GR:
			return "GR"
		case Param.NF:
			return "NF"

def get_param_value(param):
	match param:
		case Param.Rcs:
			return "@Rcs@"
		case Param.Tox:
			return "@gate_oxide_thickness@"
		case Param.Ng:
			return "@gate_dop@"
		case Param.Tg:
			return "@gate_thickness@"
		case Param.Npwb:
			return "@Pwell_bury_dop@"
		case Param.Npwt:
			return "@Pwell_top_dop@"
		case Param.Tpwb:
			return "@Pwell_bury_thickness@"
		case Param.Tpwt:
			return "@Pwell_top_thickness@"
		case Param.L:
			return "@L@"
		case Param.Npp:
			return "@Pp_dop@"
		case Param.Tpp:
			return "@Pp_thickness@"
		case Param.Lpp:
			return "@Pp_length@"
		case Param.Lsg:
			return "@source_gate_overlap@"
		case Param.Nnp:
			return "@Np_dop@"
		case Param.Tnp:
			return "@Np_thickness@"
		case Param.Lnp:
			return "@Np_length@"
		case Param.Njfet:
			return "@JFET_dop@"
		case Param.Tjfet:
			return "@JFET_thickness@"
		case Param.Ljfet:
			return "@JFET_length@"
		case Param.Nepi:
			return "@epi_dop@"
		case Param.Tepi:
			return "@epi_thickness@"
		case Param.Nsub:
			return "@sub_dop@"
		case Param.Tsub:
			return "@sub_thickness@"
		case Param.FC:
			return "@FC@"
		case Param.GR:
			return "@GR@"
		case Param.NF:
			return "@NF@"

def get_param_unit(param):
	match param:
		case Param.Rcs:
			return "Ohm"
		case Param.Tox:
			return "um"
		case Param.Ng:
			return "cm^-3"
		case Param.Tg:
			return "um"
		case Param.Npwb:
			return "cm^-3"
		case Param.Npwt:
			return "cm^-3"
		case Param.Tpwb:
			return "um"
		case Param.Tpwt:
			return "um"
		case Param.L:
			return "um"
		case Param.Npp:
			return "cm^-3"
		case Param.Tpp:
			return "um"
		case Param.Lpp:
			return "um"
		case Param.Lsg:
			return "um"
		case Param.Nnp:
			return "cm^-3"
		case Param.Tnp:
			return "um"
		case Param.Lnp:
			return "um"
		case Param.Njfet:
			return "cm^-3"
		case Param.Tjfet:
			return "um"
		case Param.Ljfet:
			return "um"
		case Param.Nepi:
			return "cm^-3"
		case Param.Tepi:
			return "um"
		case Param.Nsub:
			return "cm^-3"
		case Param.Tsub:
			return "um"
		case Param.FC:
			return "cm^-2"
		case Param.GR:
			return ""
		case Param.NF:
			return ""
