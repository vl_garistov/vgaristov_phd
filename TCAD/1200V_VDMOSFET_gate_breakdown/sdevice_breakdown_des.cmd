# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "init_vdmos_des.cmd"

Solve
{
	* Initial Solution
	Coupled
	(
		Iterations = 10000
		LineSearchDamping = 1e-3
	)
	{
		Poisson
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-3
	)
	{
		Poisson
		Electron
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-3
	)
	{
		Poisson
		Electron
		Hole
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-3
	)
	{
		Poisson
		Electron
		Hole
		* Temperature
	}

	NewCurrentPrefix = "BV_"
	Transient
	(
		InitialTime = 0.0
		FinalTime = @t_end@
		InitialStep = 1e-9
		MinStep = 1e-20
		MaxStep = 0.01
		Increment = 2.0
		Decrement = 2.5
		Goal
		{
			Name = "gate_contact"
			Voltage = 1e3
		}
		BreakCriteria
		{
			Current
			(
				contact = "gate_contact"
				AbsVal = @Ig_max@
			)
		}
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
			* Temperature
		}
		Plot
		(
			Time =
			(
				Range = (0 @t_end@)
				Intervals = 10
			)
			NoOverwrite
		)
	}
}
