; VGaristov_PhD
; Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
; Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU Affero General Public License as published
; by the Free Software Foundation, version 3.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU Affero General Public License for more details.
;
; You should have received a copy of the GNU Affero General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

; ############################## INITIALIZATION ##############################

; Clear structure
(sde:clear)
; Traditional coordinate system orientation
;(sde:set-process-up-direction "-y")
;(sdegeo:define-coord-sys "DF-ISE_custom" 0 0 -90.0)
;(sdegeo:set-active-coord-sys "DF-ISE_custom")
; New-replace-old option (default)
(sdegeo:set-default-boolean "ABA")


; ##############################   PARAMETERS   ##############################

; All dimensions are in um. All doping concentrations are in cm^-3.

; Materials
(define semiconductor_mat "4HSiC")
(define P_dopant_mat "AluminumActiveConcentration")
(define N_dopant_mat "NitrogenActiveConcentration")
(define N_gate_dopant_mat "PhosphorusActiveConcentration")
(define gate_mat "GatePolySilicon")
(define gate_oxide_mat "SiO2")
(define inter_layer_insulation_mat "SiO2")
(define moisture_barrier_mat "Si3N4")
; Polymide is not defined in Sentaurus by default
;(define top_insulation_mat "Polymide")
(define top_insulation_mat "Insulator1")
(define contact_interface_mat "NickelSilicide")
(define contact_mat "Nickel")
(define top_metal_mat "Aluminum")
(define back_metal_1_mat "Titanium")
(define back_metal_2_mat "Nickel")
(define back_metal_3_mat "Silver")

; Number of cells/fingers
(define NF @NF@)
(define GR (not (= @GR@ 0)))

; Side-deposition coefficient
; When a thin layer is deposited on a surface with peaks/valleys of greater
; height than the layer thickness, a layer is also deposited on the sides of
; the valleys. This coefficient determines the thickness of this layer as a
; fraction of the thickness that is being deposited on the surface.
(define side_deposition_coef 0.25)

; ERF lenght coefficient
; The lenght of ERF profiles is set as a fraction of the region thickness.
; This can be improved as it's not necessarily realistic.
(define erf_lenght_coef 0.2)
(define erf_side_thickness 0.04)

; Substrate parameters
; actual
;(define sub_thickness 350.0)
; for faster simulation
(define sub_thickness 3.0)
(define sub_dop (* 2.0 (expt 10 18)))

; Epitaxial layer parameters
(define epi_thickness 10.0)
(define epi_dop (* 8.0 (expt 10 15)))

; JFET parameters
;(define JFET_lenght 8.0)
(define JFET_lenght @JFET_lenght@)
(define JFET_thickness 0.65)
(define JFET_dop (* 8.0 (expt 10 15)))

; Source parameters
; Must be larger than source_gate_overlap + inter_layer_insulation_side_lenght
; Otherwise the source contact lenght becomes negative
(define Np_lenght 2.0)
(define Np_thickness 0.2)
(define Np_dop (* 1.0 (expt 10 19)))
(define source_gate_overlap 0.5)

; P-plus for P-well contact parameters
; The cell border is at the middle of the P-plus region, so the actual lenght
; of a P-plus region is twice as much.
(define Pp_lenght 0.150)
; In some figures it looks like it's not 0.7 um but 0.2 um.
(define Pp_thickness 0.2)
(define Pp_dop (* 1.0 (expt 10 19)))

; Channel lenght
(define L 0.5)

; P-well parameters
(define Pwell_lenght (+ (+ Np_lenght L) Pp_lenght))
;(define Pwell_top_thickness 0.2)
(define Pwell_top_thickness 0.15)
;(define Pwell_bury_thickness 0.45)
(define Pwell_bury_thickness 0.38)
; The total P-well thickness is Pwell_top_thickness + Pwell_bury_thickness
;(define Pwell_top_dop (* 2.0 (expt 10 17)))
(define Pwell_top_dop @Pwell_top_dop@)
(define Pwell_bury_dop (* 2.0 (expt 10 18)))

; Gate parameters
(define gate_lenght (+ JFET_lenght (* 2 (+ L source_gate_overlap))))
; Must be less than inter_layer_insulation_thickness
(define gate_thickness 0.5)
; TODO: Gate doping is a guess, find the actual value
(define gate_dop (* 1.0 (expt 10 20)))

; Inter-layer insulation parameters
(define inter_layer_insulation_side_lenght 0.5)
(define inter_layer_insulation_lenght (+ gate_lenght (* 2 inter_layer_insulation_side_lenght)))
(define inter_layer_insulation_thickness 1.0)
(define
	inter_layer_insulation_mesa_lenght
	(+ gate_lenght (* 2 (* inter_layer_insulation_thickness side_deposition_coef))))

; Gate oxide parameters
(define gate_oxide_lenght inter_layer_insulation_lenght)
;(define gate_oxide_thickness 0.05)
(define gate_oxide_thickness @gate_oxide_thickness@)

; Moisture barrier parameters
(define moisture_barrier_lenght gate_oxide_lenght)
(define moisture_barrier_thickness 0.2)
(define
	moisture_barrier_mesa_lenght
	(+
		inter_layer_insulation_mesa_lenght
		(* 2 (* moisture_barrier_thickness side_deposition_coef))))

; Source and body contact parameters
(define
	top_contact_lenght
	(+
		Pp_lenght
		(- Np_lenght (+ source_gate_overlap inter_layer_insulation_side_lenght))))
(define top_contact_thickness 0.1)

; Topside metal parameters
(define top_metal_thickness 4.0)

; Backside metalization parameters
(define back_contact_thickness 0.14)
(define back_metal_1_thickness 0.05)
(define back_metal_2_thickness 0.3)
(define back_metal_3_thickness 0.1)

; Outer P-well parameters
(define Pwell_outer_lenght 53.5)
; Compensates for the part of Pwell_outer_lenght that is part of the last cell
(define Pwell_outer_no_overlap_lenght (- Pwell_outer_lenght Pwell_lenght))
; Same as the inner P-wells
(define Pwell_outer_top_thickness Pwell_top_thickness)
(define Pwell_outer_bury_thickness Pwell_bury_thickness)
; The total P-well thickness is Pwell_top_thickness + Pwell_bury_thickness
(define Pwell_outer_top_dop Pwell_top_dop)
(define Pwell_outer_bury_dop Pwell_bury_dop)

; Outer JFET parameters
; This is a guess
(define JFET_outer_lenght Pwell_outer_lenght)
; Same as inner JFET
(define JFET_outer_thickness JFET_thickness)
(define JFET_outer_dop JFET_dop)

; Outer P-plus parameters
; Extends 4.0 um beyound the end of the outer P-well
(define Pp_outer_lenght 55.0)
; Compensates for the part of Pp_outer_lenght that is part of the last cell
(define Pp_outer_no_overlap_lenght (- Pp_outer_lenght Pp_lenght))
; Same as the inner P-plus regions
(define Pp_outer_thickness Pp_thickness)
(define Pp_outer_dop Pp_dop)

; Guard rings parameters
(define guard_rings_num 46)
(define guard_ring_lenght 2.0)
(define guard_ring_spacing 0.8)
; Same as the inner P-plus regions
(define guard_ring_thickness Pp_thickness)
(define guard_ring_dop Pp_dop)
(define guard_rings_total_lenght (* guard_rings_num (+ guard_ring_lenght guard_ring_spacing)))

; Channel stop parameters
(define channel_stop_lenght 5.0)
; Same as source
(define channel_stop_thickness Np_thickness)
(define channel_stop_dop Np_dop)
(define channel_stop_to_guard_rings_lenght 50.0)
(define channel_stop_to_die_edge_lenght 10.0)
(define
	channel_stop_total_lenght
	(+ channel_stop_lenght (+ channel_stop_to_guard_rings_lenght channel_stop_to_die_edge_lenght)))

; Outer gate oxide parameters
(define
	gate_oxide_outer_lenght
	(-
		(+ (+ channel_stop_total_lenght guard_rings_total_lenght) Pwell_outer_no_overlap_lenght)
		top_contact_lenght))
; Same as inner gate oxides
(define gate_oxide_outer_thickness gate_oxide_thickness)

; Outer gate parameters
(define gate_outer_lenght 48.0)
; Same as inner gates
; Must be less than inter_layer_insulation_outer_thickness
(define gate_outer_thickness gate_thickness)
(define gate_outer_dop gate_dop)
(define source_to_outer_gate_lenght 2.0)
; Compensates for the part of source_to_outer_gate_lenght that is part of the last cell
(define source_to_outer_gate_no_overlap_lenght (- source_to_outer_gate_lenght Pp_lenght))

; Outer inter-layer insulation parameters
; Same as inner inter-layer insulation
(define inter_layer_insulation_outer_lenght gate_oxide_outer_lenght)
(define inter_layer_insulation_outer_thickness inter_layer_insulation_thickness)
(define
	inter_layer_insulation_outer_mesa_lenght
	(+
		gate_outer_lenght
		(* 2 (* inter_layer_insulation_outer_thickness side_deposition_coef))))

; Outer moisture barrier parameters
; Same as inner moisture barrier
(define moisture_barrier_outer_lenght gate_oxide_outer_lenght)
(define moisture_barrier_outer_thickness moisture_barrier_thickness)
(define
	moisture_barrier_outer_mesa_lenght
	(+
		inter_layer_insulation_outer_mesa_lenght
		(* 2 (* moisture_barrier_outer_thickness side_deposition_coef))))

; Outer source and body contact parameters
(define top_contact_outer_lenght top_contact_lenght)
(define top_contact_outer_thickness top_contact_thickness)

; Gate runner parameters
(define gate_runner_lenght 10.0)
(define
	gate_runner_thickness
	(+ inter_layer_insulation_outer_thickness moisture_barrier_outer_thickness))
(define outer_gate_start_to_gate_runner_lenght 28.0)

; Outer top metal (above gate runner) parameters
(define top_metal_outer_lenght 20.0)
(define top_metal_outer_to_gate_runner_lenght 5.0)
(define top_metal_outer_to_top_metal_lenght 10.0)
; Same as the rest of the top metal layer
(define top_metal_outer_thickness top_metal_thickness)
(define
	top_metal_source_outer_lenght
	(-
		(+ source_to_outer_gate_no_overlap_lenght outer_gate_start_to_gate_runner_lenght)
		(+ top_metal_outer_to_gate_runner_lenght top_metal_outer_to_top_metal_lenght)))

; Cell parameters
(define cell_lenght (+ JFET_lenght (* 2 Pwell_lenght)))
(define main_cells_lenght (* cell_lenght NF))
(define cell_outer_lenght (+ gate_oxide_outer_lenght top_contact_lenght))
(define total_lenght (+ main_cells_lenght (* 2 cell_outer_lenght)))
; Extracted variables
#set AF 1.0
(display
	(string-append
		"DOE: AF "
		(string-append (number->string (* (/ 1 cell_lenght) @A@)) "\n")
	)
)

; Dielectric insulation parameters
(define top_insulation_thickness 7.0)
; TODO: This was eyeballed, find the actual value
; Causes an issue with setting the source contacts. A T-junction of edges is treated
; as three separate edges, not two. The edge which is supposed to be a contact gets split.
;(define top_insulation_lenght (+ gate_oxide_outer_lenght cell_lenght))
(define top_insulation_lenght (+ gate_oxide_outer_lenght top_contact_lenght))

; Meshing parameters
(define sub_xmax 1.0)
(define sub_ymax 5.0)
(define sub_xmin 0.1)
(define sub_ymin 0.5)
(define epi_xmax 0.5)
(define epi_ymax 1.0)
(define epi_xmin 0.005)
(define epi_ymin 0.005)
(define JFET_xmax 0.05)
(define JFET_ymax 0.1)
(define JFET_xmin 0.005)
(define JFET_ymin 0.005)
(define channel_xmax 0.005)
(define channel_ymax 0.005)
(define channel_xmin 0.0005)
(define channel_ymin 0.0001)
;(define channel_xmin 0.005)
;(define channel_ymin 0.005)
; Determines the size of the second layer of elements near an interface as a multiple
; of the size of the elements on the first layer.
(define channel_mesh_coeff 2.0)


; ##############################   Variables    ##############################

(define gate_contact_edges (list))
(define source_contact_edges (list))
(define drain_contact_edges (list))


; ##############################   MAIN CELL    ##############################

(define (build_main_cell upper_left_corner cell_number)
	(define builder_pos_x (position:x upper_left_corner))
	(define builder_pos_y (position:y upper_left_corner))
	(define first_vertex (position 0 0 0))
	(define second_vertex (position 0 0 0))
	(define new_edge (list))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_lenght) (+ builder_pos_y epi_thickness) 0)
		semiconductor_mat
		(string-append "epi_region_" (number->string cell_number)))
	(sdedr:define-refeval-window
		(string-append "epi_refeval_" (number->string cell_number))
		"Rectangle"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_lenght) (+ builder_pos_y epi_thickness) 0))
	(set! builder_pos_y (+ builder_pos_y epi_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_lenght) (+ builder_pos_y sub_thickness) 0)
		semiconductor_mat
		(string-append "sub_region_" (number->string cell_number)))
	(sdedr:define-refeval-window
		(string-append "sub_refeval_" (number->string cell_number))
		"Rectangle"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_lenght) (+ builder_pos_y sub_thickness) 0))
	(set! builder_pos_y (+ builder_pos_y sub_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_lenght) (+ builder_pos_y back_contact_thickness) 0)
		contact_mat
		(string-append "back_contact_region_" (number->string cell_number)))
	(set! builder_pos_y (+ builder_pos_y back_contact_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_lenght) (+ builder_pos_y back_metal_1_thickness) 0)
		back_metal_1_mat
		(string-append "back_metal_1_region_" (number->string cell_number)))
	(set! builder_pos_y (+ builder_pos_y back_metal_1_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_lenght) (+ builder_pos_y back_metal_2_thickness) 0)
		back_metal_2_mat
		(string-append "back_metal_2_region_" (number->string cell_number)))
	(set! builder_pos_y (+ builder_pos_y back_metal_2_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_lenght) (+ builder_pos_y back_metal_3_thickness) 0)
		back_metal_3_mat
		(string-append "back_metal_3_region_" (number->string cell_number)))
	(set! first_vertex (position builder_pos_x (+ builder_pos_y back_metal_3_thickness) 0))
	(set!
		second_vertex
		(position (+ builder_pos_x cell_lenght) (+ builder_pos_y back_metal_3_thickness) 0))
	(set! new_edge (list first_vertex second_vertex))
	(set! drain_contact_edges (append drain_contact_edges (list new_edge)))
	(set! builder_pos_y (position:y upper_left_corner))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_lenght) (+ builder_pos_y JFET_thickness) 0)
		semiconductor_mat
		(string-append "JFET_region_" (number->string cell_number)))
	(sdedr:define-refeval-window
		(string-append "JFET_refeval_" (number->string cell_number))
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_lenght) builder_pos_y 0))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Pwell_lenght) (+ builder_pos_y Pwell_top_thickness) 0)
		semiconductor_mat
		(string-append "Pwell_top_region_" (string-append (number->string cell_number) "_1")))
	(sdedr:define-refeval-window
		(string-append "Pwell_top_refeval_" (string-append (number->string cell_number) "_1"))
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Pwell_lenght) builder_pos_y 0))
	(set! builder_pos_y (+ builder_pos_y Pwell_top_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Pwell_lenght) (+ builder_pos_y Pwell_bury_thickness) 0)
		semiconductor_mat
		(string-append "Pwell_bury_region_" (string-append (number->string cell_number) "_1")))
	(sdedr:define-refeval-window
		(string-append "Pwell_bury_refeval_" (string-append (number->string cell_number) "_1"))
		"Line"
		(position builder_pos_x (+ builder_pos_y (/ Pwell_bury_thickness 2)) 0)
		(position (+ builder_pos_x Pwell_lenght) (+ builder_pos_y (/ Pwell_bury_thickness 2)) 0))
	(set! builder_pos_y (position:y upper_left_corner))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Pp_lenght) (+ builder_pos_y Pp_thickness) 0)
		semiconductor_mat
		(string-append "Pp_region_" (string-append (number->string cell_number) "_1")))
	(sdedr:define-refeval-window
		(string-append "Pp_refeval_" (string-append (number->string cell_number) "_1"))
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Pp_lenght) builder_pos_y 0))
	(set! builder_pos_x (+ builder_pos_x Pp_lenght))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Np_lenght) (+ builder_pos_y Np_thickness) 0)
		semiconductor_mat
		(string-append "Np_region_" (string-append (number->string cell_number) "_1")))
	(sdedr:define-refeval-window
		(string-append "Np_refeval_" (string-append (number->string cell_number) "_1"))
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Np_lenght) builder_pos_y 0))
	(set! builder_pos_x (+ builder_pos_x Np_lenght))
	(sdedr:define-refeval-window
		(string-append "channel_refeval_" (string-append (number->string cell_number) "_1"))
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (+ (+ builder_pos_x L) (* 2 erf_side_thickness)) builder_pos_y 0))

	(set! builder_pos_x (+ builder_pos_x (+ JFET_lenght L)))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Pwell_lenght) (+ builder_pos_y Pwell_top_thickness) 0)
		semiconductor_mat
		(string-append "Pwell_top_region_" (string-append (number->string cell_number) "_2")))
	(sdedr:define-refeval-window
		(string-append "Pwell_top_refeval_" (string-append (number->string cell_number) "_2"))
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Pwell_lenght) builder_pos_y 0))
	(set! builder_pos_y (+ builder_pos_y Pwell_top_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Pwell_lenght) (+ builder_pos_y Pwell_bury_thickness) 0)
		semiconductor_mat
		(string-append "Pwell_bury_region_" (string-append (number->string cell_number) "_2")))
	(sdedr:define-refeval-window
		(string-append "Pwell_bury_refeval_" (string-append (number->string cell_number) "_2"))
		"Line"
		(position builder_pos_x (+ builder_pos_y (/ Pwell_bury_thickness 2)) 0)
		(position (+ builder_pos_x Pwell_lenght) (+ builder_pos_y (/ Pwell_bury_thickness 2)) 0))
	(set! builder_pos_y (position:y upper_left_corner))
	(sdedr:define-refeval-window
		(string-append "channel_refeval_" (string-append (number->string cell_number) "_2"))
		"Line"
		(position ( - builder_pos_x (* 2 erf_side_thickness)) builder_pos_y 0)
		(position (+ builder_pos_x L) builder_pos_y 0))

	(set! builder_pos_x (+ builder_pos_x L))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Np_lenght) (+ builder_pos_y Np_thickness) 0)
		semiconductor_mat
		(string-append "Np_region_" (string-append (number->string cell_number) "_2")))
	(sdedr:define-refeval-window
		(string-append "Np_refeval_" (string-append (number->string cell_number) "_2"))
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Np_lenght) builder_pos_y 0))
	(set! builder_pos_x (+ builder_pos_x Np_lenght))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Pp_lenght) (+ builder_pos_y Pp_thickness) 0)
		semiconductor_mat
		(string-append "Pp_region_" (string-append (number->string cell_number) "_2")))
	(sdedr:define-refeval-window
		(string-append "Pp_refeval_" (string-append (number->string cell_number) "_2"))
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Pp_lenght) builder_pos_y 0))

	(set! builder_pos_x (position:x upper_left_corner))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x top_contact_lenght) (- builder_pos_y top_contact_thickness) 0)
		contact_mat
		(string-append "top_contact_region_" (string-append (number->string cell_number) "_1")))
	(set! builder_pos_y (- builder_pos_y top_contact_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_lenght) (- builder_pos_y top_metal_thickness) 0)
		top_metal_mat
		(string-append "top_metal_region_" (number->string cell_number)))
	(set! builder_pos_y (- builder_pos_y top_metal_thickness))
	(set! first_vertex (position builder_pos_x builder_pos_y 0))
	(set! second_vertex (position (+ builder_pos_x cell_lenght) builder_pos_y 0))
	(set! new_edge (list first_vertex second_vertex))
	(set! source_contact_edges (append source_contact_edges (list new_edge)))

	(set! builder_pos_y (position:y upper_left_corner))
	(set! builder_pos_x (+ builder_pos_x top_contact_lenght))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x gate_oxide_lenght) (- builder_pos_y gate_oxide_thickness) 0)
		gate_oxide_mat
		(string-append "gate_oxide_region_" (number->string cell_number)))
	(set! builder_pos_x (+ builder_pos_x gate_oxide_lenght))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x top_contact_lenght) (- builder_pos_y top_contact_thickness) 0)
		contact_mat
		(string-append "top_contact_region_" (string-append (number->string cell_number) "_2")))

	(set! builder_pos_x (- builder_pos_x gate_oxide_lenght))
	(set! builder_pos_y (- builder_pos_y gate_oxide_thickness))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(+ builder_pos_x inter_layer_insulation_lenght)
			(- builder_pos_y inter_layer_insulation_thickness)
			0)
		inter_layer_insulation_mat
		(string-append "inter_layer_insulation_sides_region_" (number->string cell_number)))

	; Store this location in order to return to it before building the gate
	(define gate_lower_edge_y builder_pos_y)
	(define gate_start_x (+ builder_pos_x inter_layer_insulation_side_lenght))
	(set! builder_pos_y (- builder_pos_y inter_layer_insulation_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(+ builder_pos_x moisture_barrier_lenght)
			(- builder_pos_y moisture_barrier_thickness)
			0)
		moisture_barrier_mat
		(string-append "moisture_barrier_sides_region_" (number->string cell_number)))
	(set! builder_pos_y (- builder_pos_y moisture_barrier_thickness))

	(set! builder_pos_x (+ builder_pos_x (/ (- gate_oxide_lenght moisture_barrier_mesa_lenght) 2)))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x moisture_barrier_mesa_lenght) (- builder_pos_y gate_thickness) 0)
		moisture_barrier_mat
		(string-append "moisture_barrier_region_" (number->string cell_number)))

	(set! builder_pos_y gate_lower_edge_y)
	(set! builder_pos_x gate_start_x)
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x gate_lenght) (- builder_pos_y gate_thickness) 0)
		gate_mat
		(string-append "gate_region_" (number->string cell_number)))
	(set! builder_pos_y (- builder_pos_y gate_thickness))
	(set! first_vertex (position builder_pos_x builder_pos_y 0))
	(set! second_vertex (position (+ builder_pos_x gate_lenght) builder_pos_y 0))
	(set! new_edge (list first_vertex second_vertex))
	(set! gate_contact_edges (append gate_contact_edges (list new_edge)))

	(set! builder_pos_x (- builder_pos_x (* inter_layer_insulation_thickness side_deposition_coef)))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(+ builder_pos_x inter_layer_insulation_mesa_lenght)
			(- builder_pos_y inter_layer_insulation_thickness)
			0)
		inter_layer_insulation_mat
		(string-append "inter_layer_insulation_region_" (number->string cell_number)))
)


; ##############################   OUTER CELL   ##############################

(define (build_right_outer_cell upper_left_corner)
	(define builder_pos_x (position:x upper_left_corner))
	(define builder_pos_y (position:y upper_left_corner))
	(define first_vertex (position 0 0 0))
	(define second_vertex (position 0 0 0))
	(define new_edge (list))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y epi_thickness) 0)
		semiconductor_mat
		"epi_region_R")
	(sdedr:define-refeval-window
		"epi_refeval_R"
		"Rectangle"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y epi_thickness) 0))
	(set! builder_pos_y (+ builder_pos_y epi_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y sub_thickness) 0)
		semiconductor_mat
		"sub_region_R")
	(sdedr:define-refeval-window
		"sub_refeval_R"
		"Rectangle"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y sub_thickness) 0))
	(set! builder_pos_y (+ builder_pos_y sub_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y back_contact_thickness) 0)
		contact_mat
		"back_contact_region_R")
	(set! builder_pos_y (+ builder_pos_y back_contact_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y back_metal_1_thickness) 0)
		back_metal_1_mat
		"back_metal_1_region_R")
	(set! builder_pos_y (+ builder_pos_y back_metal_1_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y back_metal_2_thickness) 0)
		back_metal_2_mat
		"back_metal_2_region_R")
	(set! builder_pos_y (+ builder_pos_y back_metal_2_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y back_metal_3_thickness) 0)
		back_metal_3_mat
		"back_metal_3_region_R")
	(set! first_vertex (position builder_pos_x (+ builder_pos_y back_metal_3_thickness) 0))
	(set!
		second_vertex
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y back_metal_3_thickness) 0))
	(set! new_edge (list first_vertex second_vertex))
	(set! drain_contact_edges (append drain_contact_edges (list new_edge)))
	(set! builder_pos_y (position:y upper_left_corner))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x JFET_outer_lenght) (+ builder_pos_y JFET_outer_thickness) 0)
		semiconductor_mat
		"JFET_region_R")
	(sdedr:define-refeval-window
		"JFET_refeval_R"
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x JFET_outer_lenght) builder_pos_y 0))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(+ builder_pos_x Pwell_outer_no_overlap_lenght)
			(+ builder_pos_y Pwell_outer_top_thickness)
			0)
		semiconductor_mat
		"Pwell_top_region_R")
	(sdedr:define-refeval-window
		"Pwell_top_refeval_R"
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Pwell_outer_no_overlap_lenght) builder_pos_y 0))
	(set! builder_pos_y (+ builder_pos_y Pwell_outer_top_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(+ builder_pos_x Pwell_outer_no_overlap_lenght)
			(+ builder_pos_y Pwell_outer_bury_thickness)
			0)
		semiconductor_mat
		"Pwell_bury_region_R")
	(sdedr:define-refeval-window
		"Pwell_bury_refeval_R"
		"Line"
		(position builder_pos_x (+ builder_pos_y (/ Pwell_outer_bury_thickness 2)) 0)
		(position (+ builder_pos_x Pwell_outer_no_overlap_lenght) (+ builder_pos_y (/ Pwell_outer_bury_thickness 2)) 0))
	(set! builder_pos_y (position:y upper_left_corner))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Pp_outer_no_overlap_lenght) (+ builder_pos_y Pp_outer_thickness) 0)
		semiconductor_mat
		"Pp_region_R")
	(sdedr:define-refeval-window
		"Pp_refeval_R"
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x Pp_outer_no_overlap_lenght) builder_pos_y 0))
	(set! builder_pos_x (+ builder_pos_x Pp_outer_no_overlap_lenght))

	(do ( (i 0 (+ i 1)) )
		( (= i guard_rings_num) )
		(begin
			(set! builder_pos_x (+ builder_pos_x guard_ring_spacing))
			(sdegeo:create-rectangle
				(position builder_pos_x builder_pos_y 0)
				(position (+ builder_pos_x guard_ring_lenght) (+ builder_pos_y guard_ring_thickness) 0)
				semiconductor_mat
				(string-append "guard_ring_region_R_" (number->string i)))
			(sdedr:define-refeval-window
				(string-append "guard_ring_refeval_R_" (number->string i))
				"Line"
				(position builder_pos_x builder_pos_y 0)
				(position (+ builder_pos_x guard_ring_lenght) builder_pos_y 0))
			(set! builder_pos_x (+ builder_pos_x guard_ring_lenght))
		)
	)

	(set! builder_pos_x (+ builder_pos_x channel_stop_to_guard_rings_lenght))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x channel_stop_lenght) (+ builder_pos_y channel_stop_thickness) 0)
		semiconductor_mat
		"channel_stop_region_R")
	(sdedr:define-refeval-window
		"channel_stop_refeval_R"
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x channel_stop_lenght) builder_pos_y 0))
	
	(set! builder_pos_x (position:x upper_left_corner))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(+ builder_pos_x top_contact_outer_lenght)
			(- builder_pos_y top_contact_outer_thickness)
			0)
		contact_mat
		"top_contact_region_R")
	
	(set!
		builder_pos_y
		(-
			builder_pos_y
			(+
				(+ gate_oxide_outer_thickness inter_layer_insulation_outer_thickness)
				moisture_barrier_outer_thickness)))
	(set! builder_pos_x (- (+ builder_pos_x cell_outer_lenght) top_insulation_lenght))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x top_insulation_lenght) (- builder_pos_y top_insulation_thickness) 0)
		top_insulation_mat
		"top_insulation_region_R")

	(set! builder_pos_x (position:x upper_left_corner))
	(set! builder_pos_y (- (position:y upper_left_corner) top_contact_outer_thickness))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(+ builder_pos_x top_metal_source_outer_lenght)
			(- builder_pos_y top_metal_outer_thickness)
			0)
		top_metal_mat
		"top_metal_source_region_R")
	(set! builder_pos_y (- builder_pos_y top_metal_outer_thickness))
	(set! first_vertex (position builder_pos_x builder_pos_y 0))
	(set! second_vertex (position (+ builder_pos_x top_metal_source_outer_lenght) builder_pos_y 0))
	(set! new_edge (list first_vertex second_vertex))
	(set! source_contact_edges (append source_contact_edges (list new_edge)))

	(set! builder_pos_y (+ builder_pos_y top_metal_outer_thickness))
	(set!
		builder_pos_x
		(+ builder_pos_x (+ top_metal_outer_to_top_metal_lenght top_metal_source_outer_lenght)))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(+ builder_pos_x top_metal_outer_lenght)
			(- builder_pos_y top_metal_outer_thickness)
			0)
		top_metal_mat
		"top_metal_outer_region_R")
	(set! builder_pos_y (- builder_pos_y top_metal_outer_thickness))
	(set! first_vertex (position builder_pos_x builder_pos_y 0))
	(set! second_vertex (position (+ builder_pos_x top_metal_outer_lenght) builder_pos_y 0))
	(set! new_edge (list first_vertex second_vertex))
	(set! gate_contact_edges (append gate_contact_edges (list new_edge)))

	(set! builder_pos_x (+ (position:x upper_left_corner) top_contact_outer_lenght))
	(set! builder_pos_y (position:y upper_left_corner))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(+ builder_pos_x gate_oxide_outer_lenght)
			(- builder_pos_y gate_oxide_outer_thickness)
			0)
		gate_oxide_mat
		"gate_oxide_region_R")
	(set! builder_pos_y (- builder_pos_y gate_oxide_outer_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(+ builder_pos_x inter_layer_insulation_outer_lenght)
			(- builder_pos_y inter_layer_insulation_outer_thickness)
			0)
		inter_layer_insulation_mat
		"inter_layer_insulation_sides_region_R")
	(set! builder_pos_y (- builder_pos_y inter_layer_insulation_outer_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(+ builder_pos_x moisture_barrier_outer_lenght)
			(- builder_pos_y moisture_barrier_outer_thickness)
			0)
		moisture_barrier_mat
		"moisture_barrier_sides_region_R")
	(set! builder_pos_y (- builder_pos_y moisture_barrier_outer_thickness))

	(set! builder_pos_x (+ (position:x upper_left_corner) source_to_outer_gate_no_overlap_lenght))
	(set!
		builder_pos_x
		(- builder_pos_x (/ (- moisture_barrier_outer_mesa_lenght gate_outer_lenght) 2)))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(+ builder_pos_x moisture_barrier_outer_mesa_lenght)
			(- builder_pos_y gate_outer_thickness)
			0)
		moisture_barrier_mat
		"moisture_barrier_region_R")

	(set! builder_pos_y (- (position:y upper_left_corner) gate_oxide_outer_thickness))
	(set! builder_pos_x (+ (position:x upper_left_corner) source_to_outer_gate_no_overlap_lenght))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x gate_outer_lenght) (- builder_pos_y gate_outer_thickness) 0)
		gate_mat
		"gate_region_R")
	(set! builder_pos_y (- builder_pos_y gate_outer_thickness))

	; Save this position to return to it after building the inter-layer insulation
	(define gate_start_x builder_pos_x)

	(set!
		builder_pos_x
		(- builder_pos_x (* inter_layer_insulation_outer_thickness side_deposition_coef)))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(+ builder_pos_x inter_layer_insulation_outer_mesa_lenght)
			(- builder_pos_y inter_layer_insulation_outer_thickness)
			0)
		inter_layer_insulation_mat
		"inter_layer_insulation_region_R")

	(set! builder_pos_x (+ gate_start_x outer_gate_start_to_gate_runner_lenght))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x gate_runner_lenght) (- builder_pos_y gate_runner_thickness) 0)
		top_metal_mat
		"gate_runner_region_R")
)

; Derived from build_right_outer_cell by inverting the sign of some values
(define (build_left_outer_cell upper_left_corner)
	(define builder_pos_x (position:x upper_left_corner))
	(define builder_pos_y (position:y upper_left_corner))
	(define first_vertex (position 0 0 0))
	(define second_vertex (position 0 0 0))
	(define new_edge (list))
	; (define upper_right_corner upper_left_corner) makes a reference
	(define upper_right_corner (position 0 0 0))
	(position:set-x! upper_right_corner (position:x upper_left_corner))
	(position:set-y! upper_right_corner (position:y upper_left_corner))
	(position:set-z! upper_right_corner (position:z upper_left_corner))
	(position:set-x! upper_right_corner (+ (position:x upper_right_corner) cell_outer_lenght))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y epi_thickness) 0)
		semiconductor_mat
		(string-append "epi_region_L"))
	(sdedr:define-refeval-window
		"epi_refeval_L"
		"Rectangle"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y epi_thickness) 0))
	(set! builder_pos_y (+ builder_pos_y epi_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y sub_thickness) 0)
		semiconductor_mat
		(string-append "sub_region_L"))
	(sdedr:define-refeval-window
		"sub_refeval_L"
		"Rectangle"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y sub_thickness) 0))
	(set! builder_pos_y (+ builder_pos_y sub_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y back_contact_thickness) 0)
		contact_mat
		(string-append "back_contact_region_L"))
	(set! builder_pos_y (+ builder_pos_y back_contact_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y back_metal_1_thickness) 0)
		back_metal_1_mat
		(string-append "back_metal_1_region_L"))
	(set! builder_pos_y (+ builder_pos_y back_metal_1_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y back_metal_2_thickness) 0)
		back_metal_2_mat
		(string-append "back_metal_2_region_L"))
	(set! builder_pos_y (+ builder_pos_y back_metal_2_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y back_metal_3_thickness) 0)
		back_metal_3_mat
		(string-append "back_metal_3_region_L"))
	(set! first_vertex (position builder_pos_x (+ builder_pos_y back_metal_3_thickness) 0))
	(set!
		second_vertex
		(position (+ builder_pos_x cell_outer_lenght) (+ builder_pos_y back_metal_3_thickness) 0))
	(set! new_edge (list first_vertex second_vertex))
	(set! drain_contact_edges (append drain_contact_edges (list new_edge)))
	(set! builder_pos_y (position:y upper_right_corner))

	(set! builder_pos_x (position:x upper_right_corner))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (- builder_pos_x JFET_outer_lenght) (+ builder_pos_y JFET_outer_thickness) 0)
		semiconductor_mat
		"JFET_region_L")
	(sdedr:define-refeval-window
		"JFET_refeval_L"
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (- builder_pos_x JFET_outer_lenght) builder_pos_y 0))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(- builder_pos_x Pwell_outer_no_overlap_lenght)
			(+ builder_pos_y Pwell_outer_top_thickness)
			0)
		semiconductor_mat
		"Pwell_top_region_L")
	(sdedr:define-refeval-window
		"Pwell_top_refeval_L"
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (- builder_pos_x Pwell_outer_no_overlap_lenght) builder_pos_y 0))
	(set! builder_pos_y (+ builder_pos_y Pwell_outer_top_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(- builder_pos_x Pwell_outer_no_overlap_lenght)
			(+ builder_pos_y Pwell_outer_bury_thickness)
			0)
		semiconductor_mat
		"Pwell_bury_region_L")
	(sdedr:define-refeval-window
		"Pwell_bury_refeval_L"
		"Line"
		(position builder_pos_x (+ builder_pos_y (/ Pwell_outer_bury_thickness 2)) 0)
		(position (- builder_pos_x Pwell_outer_no_overlap_lenght) (+ builder_pos_y (/ Pwell_outer_bury_thickness 2)) 0))
	(set! builder_pos_y (position:y upper_right_corner))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (- builder_pos_x Pp_outer_no_overlap_lenght) (+ builder_pos_y Pp_outer_thickness) 0)
		semiconductor_mat
		"Pp_region_L")
	(sdedr:define-refeval-window
		"Pp_refeval_L"
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (- builder_pos_x Pp_outer_no_overlap_lenght) builder_pos_y 0))
	(set! builder_pos_x (- builder_pos_x Pp_outer_no_overlap_lenght))

	(do ( (i 0 (+ i 1)) )
		( (= i guard_rings_num) )
		(begin
			(set! builder_pos_x (- builder_pos_x guard_ring_spacing))
			(sdegeo:create-rectangle
				(position builder_pos_x builder_pos_y 0)
				(position (- builder_pos_x guard_ring_lenght) (+ builder_pos_y guard_ring_thickness) 0)
				semiconductor_mat
				(string-append "guard_ring_region_L_" (number->string i)))
			(sdedr:define-refeval-window
				(string-append "guard_ring_refeval_L_" (number->string i))
				"Line"
				(position builder_pos_x builder_pos_y 0)
				(position (- builder_pos_x guard_ring_lenght) builder_pos_y 0))
			(set! builder_pos_x (- builder_pos_x guard_ring_lenght))
		)
	)

	(set! builder_pos_x (- builder_pos_x channel_stop_to_guard_rings_lenght))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (- builder_pos_x channel_stop_lenght) (+ builder_pos_y channel_stop_thickness) 0)
		semiconductor_mat
		"channel_stop_region_L")
	(sdedr:define-refeval-window
		"channel_stop_refeval_L"
		"Line"
		(position builder_pos_x builder_pos_y 0)
		(position (+ builder_pos_x channel_stop_lenght) builder_pos_y 0))
	
	(set! builder_pos_x (position:x upper_right_corner))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(- builder_pos_x top_contact_outer_lenght)
			(- builder_pos_y top_contact_outer_thickness)
			0)
		contact_mat
		"top_contact_region_L")
	
	(set!
		builder_pos_y
		(-
			builder_pos_y
			(+
				(+ gate_oxide_outer_thickness inter_layer_insulation_outer_thickness)
				moisture_barrier_outer_thickness)))
	(set! builder_pos_x (+ (- builder_pos_x cell_outer_lenght) top_insulation_lenght))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (- builder_pos_x top_insulation_lenght) (- builder_pos_y top_insulation_thickness) 0)
		top_insulation_mat
		"top_insulation_region_L")

	(set! builder_pos_x (position:x upper_right_corner))
	(set! builder_pos_y (- (position:y upper_right_corner) top_contact_outer_thickness))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(- builder_pos_x top_metal_source_outer_lenght)
			(- builder_pos_y top_metal_outer_thickness)
			0)
		top_metal_mat
		"top_metal_source_region_L")
	(set! builder_pos_y (- builder_pos_y top_metal_outer_thickness))
	(set! first_vertex (position builder_pos_x builder_pos_y 0))
	(set! second_vertex (position (- builder_pos_x top_metal_source_outer_lenght) builder_pos_y 0))
	(set! new_edge (list first_vertex second_vertex))
	(set! source_contact_edges (append source_contact_edges (list new_edge)))

	(set! builder_pos_y (+ builder_pos_y top_metal_outer_thickness))
	(set!
		builder_pos_x
		(- builder_pos_x (+ top_metal_outer_to_top_metal_lenght top_metal_source_outer_lenght)))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(- builder_pos_x top_metal_outer_lenght)
			(- builder_pos_y top_metal_outer_thickness)
			0)
		top_metal_mat
		"top_metal_outer_region_L")
	(set! builder_pos_y (- builder_pos_y top_metal_outer_thickness))
	(set! first_vertex (position builder_pos_x builder_pos_y 0))
	(set! second_vertex (position (- builder_pos_x top_metal_outer_lenght) builder_pos_y 0))
	(set! new_edge (list first_vertex second_vertex))
	(set! gate_contact_edges (append gate_contact_edges (list new_edge)))

	(set! builder_pos_x (- (position:x upper_right_corner) top_contact_outer_lenght))
	(set! builder_pos_y (position:y upper_right_corner))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(- builder_pos_x gate_oxide_outer_lenght)
			(- builder_pos_y gate_oxide_outer_thickness)
			0)
		gate_oxide_mat
		"gate_oxide_region_L")
	(set! builder_pos_y (- builder_pos_y gate_oxide_outer_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(- builder_pos_x inter_layer_insulation_outer_lenght)
			(- builder_pos_y inter_layer_insulation_outer_thickness)
			0)
		inter_layer_insulation_mat
		"inter_layer_insulation_sides_region_L")
	(set! builder_pos_y (- builder_pos_y inter_layer_insulation_outer_thickness))

	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(- builder_pos_x moisture_barrier_outer_lenght)
			(- builder_pos_y moisture_barrier_outer_thickness)
			0)
		moisture_barrier_mat
		"moisture_barrier_sides_region_L")
	(set! builder_pos_y (- builder_pos_y moisture_barrier_outer_thickness))

	(set! builder_pos_x (- (position:x upper_right_corner) source_to_outer_gate_no_overlap_lenght))
	(set!
		builder_pos_x
		(+ builder_pos_x (/ (- moisture_barrier_outer_mesa_lenght gate_outer_lenght) 2)))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(- builder_pos_x moisture_barrier_outer_mesa_lenght)
			(- builder_pos_y gate_outer_thickness)
			0)
		moisture_barrier_mat
		"moisture_barrier_region_L")

	(set! builder_pos_y (- (position:y upper_right_corner) gate_oxide_outer_thickness))
	(set! builder_pos_x (- (position:x upper_right_corner) source_to_outer_gate_no_overlap_lenght))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (- builder_pos_x gate_outer_lenght) (- builder_pos_y gate_outer_thickness) 0)
		gate_mat
		"gate_region_L")
	(set! builder_pos_y (- builder_pos_y gate_outer_thickness))

	; Save this position to return to it after building the inter-layer insulation
	(define gate_start_x builder_pos_x)

	(set!
		builder_pos_x
		(+ builder_pos_x (* inter_layer_insulation_outer_thickness side_deposition_coef)))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position
			(- builder_pos_x inter_layer_insulation_outer_mesa_lenght)
			(- builder_pos_y inter_layer_insulation_outer_thickness)
			0)
		inter_layer_insulation_mat
		"inter_layer_insulation_region_L")

	(set! builder_pos_x (- gate_start_x outer_gate_start_to_gate_runner_lenght))
	(sdegeo:create-rectangle
		(position builder_pos_x builder_pos_y 0)
		(position (- builder_pos_x gate_runner_lenght) (- builder_pos_y gate_runner_thickness) 0)
		top_metal_mat
		"gate_runner_region_L")
)


; ##############################  FULL DEVICE   ##############################

(define (build_device num_cells build_outer)

	; ########## GEOMETRY ##########

	(define next_cell_start (position 0 0 0))

	(if build_outer
		(begin
			(build_left_outer_cell next_cell_start)
			(position:set-x!
				next_cell_start
				(+ (position:x next_cell_start) (+ cell_outer_lenght main_cells_lenght)))
			(build_right_outer_cell next_cell_start)
			(position:set-x! next_cell_start (- (position:x next_cell_start) main_cells_lenght))
		)
	)

	(do ( (i 0 (+ i 1)) )
		( (= i num_cells) )
		(begin
			(build_main_cell next_cell_start i)
			(position:set-x! next_cell_start (+ (position:x next_cell_start) cell_lenght))
		)
	)


	; ########## CONTACTS ##########

	; Drain
	(sdegeo:define-contact-set "drain_contact" 2 (color:rgb 1.0 0.0 0.0) "##")
	(sdegeo:set-current-contact-set "drain_contact")
	(for-each
		(lambda (edge)
			(begin
				(sdegeo:set-contact (find-edge-id (car edge) (find-edge-id (list-ref edge 1))))
			)
		) drain_contact_edges
	)

	; Source
	(sdegeo:define-contact-set "source_contact" 2 (color:rgb 0.0 0.0 1.0) "##")
	(sdegeo:set-current-contact-set "source_contact")
	(for-each
		(lambda (edge)
			(begin
				(sdegeo:set-contact (find-edge-id (car edge) (find-edge-id (list-ref edge 1))))
			)
		) source_contact_edges
	)

	; Gate
	(sdegeo:define-contact-set "gate_contact" 2 (color:rgb 0.0 1.0 0.0) "##")
	(sdegeo:set-current-contact-set "gate_contact")
	(for-each
		(lambda (edge)
			(begin
				(sdegeo:set-contact (find-edge-id (car edge) (find-edge-id (list-ref edge 1))))
			)
		) gate_contact_edges
	)


	; ########## DOPING ##########

	; Doping profiles
	(sdedr:define-constant-profile "sub_dop_profile" N_dopant_mat sub_dop)
	(sdedr:define-constant-profile "epi_dop_profile" N_dopant_mat epi_dop)
	(sdedr:define-constant-profile "gate_dop_profile" N_gate_dopant_mat gate_dop)
	(sdedr:define-constant-profile "gate_outer_dop_profile" N_gate_dopant_mat gate_outer_dop)
	(sdedr:define-erf-profile
		"JFET_dop_profile"
		N_dopant_mat
		"SymPos" JFET_thickness
		"MaxVal" JFET_dop
		"Length" (* erf_lenght_coef JFET_thickness)
		"Erf"
		"Length" erf_side_thickness)
		;"Length" (* erf_lenght_coef JFET_thickness))
	(sdedr:define-erf-profile
		"JFET_outer_dop_profile"
		N_dopant_mat
		"SymPos" JFET_outer_thickness
		"MaxVal" JFET_outer_dop
		"Length" (* erf_lenght_coef JFET_outer_thickness)
		"Erf"
		"Length" erf_side_thickness)
		;"Length" (* erf_lenght_coef JFET_outer_thickness))
	(sdedr:define-erf-profile
		"Pwell_top_dop_profile"
		P_dopant_mat
		"SymPos" Pwell_top_thickness
		"MaxVal" Pwell_top_dop
		"Length" (* erf_lenght_coef Pwell_top_thickness)
		"Erf"
		"Length" erf_side_thickness)
		;"Length" (* erf_lenght_coef Pwell_top_thickness))
	(sdedr:define-erf-profile
		"Pwell_outer_top_dop_profile"
		P_dopant_mat
		"SymPos" Pwell_outer_top_thickness
		"MaxVal" Pwell_outer_top_dop
		"Length" (* erf_lenght_coef Pwell_outer_top_thickness)
		"Erf"
		"Length" erf_side_thickness)
		;"Length" (* erf_lenght_coef Pwell_outer_top_thickness))
	(sdedr:define-erf-profile
		"Pwell_bury_dop_profile"
		P_dopant_mat
		"SymPos" (/ Pwell_bury_thickness 2)
		"MaxVal" Pwell_bury_dop
		"Length" (* erf_lenght_coef Pwell_bury_thickness)
		"Erf"
		"Length" erf_side_thickness)
		;"Length" (* erf_lenght_coef Pwell_bury_thickness))
	(sdedr:define-erf-profile
		"Pwell_outer_bury_dop_profile"
		P_dopant_mat
		"SymPos" (/ Pwell_outer_bury_thickness 2)
		"MaxVal" Pwell_outer_bury_dop
		"Length" (* erf_lenght_coef Pwell_outer_bury_thickness)
		"Erf"
		"Length" erf_side_thickness)
		;"Length" (* erf_lenght_coef Pwell_outer_bury_thickness))
	(sdedr:define-erf-profile
		"Pp_dop_profile"
		P_dopant_mat
		"SymPos" Pp_thickness
		"MaxVal" Pp_dop
		"Length" (* erf_lenght_coef Pp_thickness)
		"Erf"
		"Length" erf_side_thickness)
		;"Length" (* erf_lenght_coef Pp_thickness))
	(sdedr:define-erf-profile
		"Pp_outer_dop_profile"
		P_dopant_mat
		"SymPos" Pp_outer_thickness
		"MaxVal" Pp_outer_dop
		"Length" (* erf_lenght_coef Pp_outer_thickness)
		"Erf"
		"Length" erf_side_thickness)
		;"Length" (* erf_lenght_coef Pp_outer_thickness))
	(sdedr:define-erf-profile
		"Np_dop_profile"
		N_dopant_mat
		"SymPos" Np_thickness
		"MaxVal" Np_dop
		"Length" (* erf_lenght_coef Np_thickness)
		"Erf"
		"Length" erf_side_thickness)
		;"Length" (* erf_lenght_coef Np_thickness))
	(sdedr:define-erf-profile
		"guard_ring_dop_profile"
		P_dopant_mat
		"SymPos" guard_ring_thickness
		"MaxVal" guard_ring_dop
		"Length" (* erf_lenght_coef guard_ring_thickness)
		"Erf"
		"Length" erf_side_thickness)
		;"Length" (* erf_lenght_coef guard_ring_thickness))
	(sdedr:define-erf-profile
		"channel_stop_dop_profile"
		N_dopant_mat
		"SymPos" channel_stop_thickness
		"MaxVal" channel_stop_dop
		"Length" (* erf_lenght_coef channel_stop_thickness)
		"Erf"
		"Length" erf_side_thickness)
		;"Length" (* erf_lenght_coef channel_stop_thickness))
	
	(if build_outer
		(begin
			; Outer substrate
			(sdedr:define-constant-profile-placement "sub_dop_placement_L" "sub_dop_profile" "sub_refeval_L")
			(sdedr:define-constant-profile-placement "sub_dop_placement_R" "sub_dop_profile" "sub_refeval_R")
			; Outer epitaxial layer
			(sdedr:define-constant-profile-placement "epi_dop_placement_L" "epi_dop_profile" "epi_refeval_L")
			(sdedr:define-constant-profile-placement "epi_dop_placement_R" "epi_dop_profile" "epi_refeval_R")
			; Outer gate
			(sdedr:define-constant-profile-placement "gate_dop_placement_L" "gate_outer_dop_profile" "gate_refeval_L")
			(sdedr:define-constant-profile-placement "gate_dop_placement_R" "gate_outer_dop_profile" "gate_refeval_R")
			; Outer JFET
			(sdedr:define-analytical-profile-placement
				"JFET_outer_dop_placement_L" "JFET_outer_dop_profile" "JFET_refeval_L"
				"Positive" "NoReplace" "Eval")
			(sdedr:define-analytical-profile-placement
				"JFET_outer_dop_placement_R" "JFET_outer_dop_profile" "JFET_refeval_R"
				"Positive" "NoReplace" "Eval")
			; Outer burried P-well
			(sdedr:define-analytical-profile-placement
				"Pwell_outer_bury_dop_placement_L" "Pwell_outer_bury_dop_profile" "Pwell_bury_refeval_L"
				"Both" "NoReplace" "Eval")
			(sdedr:define-analytical-profile-placement
				"Pwell_outer_bury_dop_placement_R" "Pwell_outer_bury_dop_profile" "Pwell_bury_refeval_R"
				"Both" "NoReplace" "Eval")
			; Outer P-well
			(sdedr:define-analytical-profile-placement
				"Pwell_outer_top_dop_placement_L" "Pwell_outer_top_dop_profile" "Pwell_top_refeval_L"
				"Positive" "NoReplace" "Eval")
			(sdedr:define-analytical-profile-placement
				"Pwell_outer_top_dop_placement_R" "Pwell_outer_top_dop_profile" "Pwell_top_refeval_R"
				"Positive" "NoReplace" "Eval")
			; Outer body
			(sdedr:define-analytical-profile-placement
				"Pp_outer_dop_placement_L" "Pp_outer_dop_profile" "Pp_refeval_L"
				"Positive" "NoReplace" "Eval")
			(sdedr:define-analytical-profile-placement
				"Pp_outer_dop_placement_R" "Pp_outer_dop_profile" "Pp_refeval_R"
				"Positive" "NoReplace" "Eval")
			; Channel stop
			(sdedr:define-analytical-profile-placement
				"channel_stop_dop_placement_L" "channel_stop_dop_profile" "channel_stop_refeval_L"
				"Positive" "NoReplace" "Eval")
			(sdedr:define-analytical-profile-placement
				"channel_stop_dop_placement_R" "channel_stop_dop_profile" "channel_stop_refeval_R"
				"Positive" "NoReplace" "Eval")

			; Guard rings
			(do ( (ring 0 (+ ring 1)) )
				( (= ring guard_rings_num) )
				(begin
					(sdedr:define-analytical-profile-placement
						(string-append "guard_ring_dop_placement_L_" (number->string ring))
						"guard_ring_dop_profile"
						(string-append "guard_ring_refeval_L_" (number->string ring))
						"Positive" "NoReplace" "Eval")
					(sdedr:define-analytical-profile-placement
						(string-append "guard_ring_dop_placement_R_" (number->string ring))
						"guard_ring_dop_profile"
						(string-append "guard_ring_refeval_R_" (number->string ring))
						"Positive" "NoReplace" "Eval")
				)
			)
		)
	)

	; Main cells
	(do ( (cell 0 (+ cell 1)) )
		( (= cell num_cells) )
		(begin
			; Substrate
			(sdedr:define-constant-profile-region
				(string-append "sub_dop_placement_" (number->string cell))
				"sub_dop_profile"
				(string-append "sub_region_" (number->string cell)))
			; Epitaxial layer
			(sdedr:define-constant-profile-region
				(string-append "epi_dop_placement_" (number->string cell))
				"epi_dop_profile"
				(string-append "epi_region_" (number->string cell)))
			; Gate
			(sdedr:define-constant-profile-region
				(string-append "gate_dop_placement_" (number->string cell))
				"gate_dop_profile"
				(string-append "gate_region_" (number->string cell)))
			; JFET
			(sdedr:define-analytical-profile-placement
				(string-append "JFET_dop_placement_" (number->string cell))
				"JFET_dop_profile"
				(string-append "JFET_refeval_" (number->string cell))
				"Positive" "NoReplace" "Eval")
			; Burried P-well
			(sdedr:define-analytical-profile-placement
				(string-append (string-append "Pwell_bury_dop_placement_" (number->string cell)) "_1")
				"Pwell_bury_dop_profile"
				(string-append (string-append "Pwell_bury_refeval_" (number->string cell)) "_1")
				"Both" "NoReplace" "Eval")
			(sdedr:define-analytical-profile-placement
				(string-append (string-append "Pwell_bury_dop_placement_" (number->string cell)) "_2")
				"Pwell_bury_dop_profile"
				(string-append (string-append "Pwell_bury_refeval_" (number->string cell)) "_2")
				"Both" "NoReplace" "Eval")
			; P-well
			(sdedr:define-analytical-profile-placement
				(string-append (string-append "Pwell_top_dop_placement_" (number->string cell)) "_1")
				"Pwell_top_dop_profile"
				(string-append (string-append "Pwell_top_refeval_" (number->string cell)) "_1")
				"Positive" "NoReplace" "Eval")
			(sdedr:define-analytical-profile-placement
				(string-append (string-append "Pwell_top_dop_placement_" (number->string cell)) "_2")
				"Pwell_top_dop_profile"
				(string-append (string-append "Pwell_top_refeval_" (number->string cell)) "_2")
				"Positive" "NoReplace" "Eval")
			; Body
			(sdedr:define-analytical-profile-placement
				(string-append (string-append "Pp_dop_placement_" (number->string cell)) "_1")
				"Pp_dop_profile"
				(string-append (string-append "Pp_refeval_" (number->string cell)) "_1")
				"Positive" "NoReplace" "Eval")
			(sdedr:define-analytical-profile-placement
				(string-append (string-append "Pp_dop_placement_" (number->string cell)) "_2")
				"Pp_dop_profile"
				(string-append (string-append "Pp_refeval_" (number->string cell)) "_2")
				"Positive" "NoReplace" "Eval")
			; Source
			(sdedr:define-analytical-profile-placement
				(string-append (string-append "Np_dop_placement_" (number->string cell)) "_1")
				"Np_dop_profile"
				(string-append (string-append "Np_refeval_" (number->string cell)) "_1")
				"Positive" "NoReplace" "Eval")
			(sdedr:define-analytical-profile-placement
				(string-append (string-append "Np_dop_placement_" (number->string cell)) "_2")
				"Np_dop_profile"
				(string-append (string-append "Np_refeval_" (number->string cell)) "_2")
				"Positive" "NoReplace" "Eval")
		)
	)
)


; ##############################    MESHING     ##############################

(define (refine_and_mesh num_cells build_outer)
	; Refinement sizes and functions
	(sdedr:define-refinement-size "sub_refinement" sub_xmax sub_ymax sub_xmin sub_ymin)
	(sdedr:define-refinement-function "sub_refinement" "DopingConcentration" "MaxTransDiff" 1)
	(sdedr:define-refinement-size "epi_refinement" epi_xmax epi_ymax epi_xmin epi_ymin)
	(sdedr:define-refinement-function "epi_refinement" "DopingConcentration" "MaxTransDiff" 1)
	(sdedr:define-refinement-size "JFET_refinement" JFET_xmax JFET_ymax JFET_xmin JFET_ymin)
	(sdedr:define-refinement-function "JFET_refinement" "DopingConcentration" "MaxTransDiff" 1)
	(sdedr:define-refinement-size
		"channel_refinement"
		channel_xmax channel_ymax
		channel_xmin channel_ymin)
	(sdedr:define-refinement-function
		"channel_refinement"
		"MaxLenInt"
		semiconductor_mat gate_oxide_mat
		channel_ymin channel_mesh_coeff)

	(if build_outer
		(begin
			(sdedr:define-refinement-placement "sub_refinement_placement_L" "sub_refinement" "sub_refeval_L")
			(sdedr:define-refinement-placement "sub_refinement_placement_R" "sub_refinement" "sub_refeval_R")
			(sdedr:define-refinement-placement "epi_refinement_placement_L" "epi_refinement" "epi_refeval_L")
			(sdedr:define-refinement-placement "epi_refinement_placement_R" "epi_refinement" "epi_refeval_R")
			(sdedr:define-refinement-region "JFET_refinement_placement_L" "JFET_refinement" "JFET_region_L")
			(sdedr:define-refinement-region "JFET_refinement_placement_R" "JFET_refinement" "JFET_region_R")
		)
	)

	(do ( (cell 0 (+ cell 1)) )
		( (= cell num_cells) )
		(begin
			; Substrate refinement
			(sdedr:define-refinement-placement
				(string-append "sub_refinement_placement_" (number->string cell))
				"sub_refinement"
				(string-append "sub_refeval_" (number->string cell)))
			; Epitaxy refinement
			(sdedr:define-refinement-placement
				(string-append "epi_refinement_placement_" (number->string cell))
				"epi_refinement"
				(string-append "epi_refeval_" (number->string cell)))
			; JFET refinement
			(sdedr:define-refinement-region
				(string-append "JFET_refinement_placement_" (number->string cell))
				"JFET_refinement"
				(string-append "JFET_region_" (number->string cell)))
			; Channel refinements
			(sdedr:define-refinement-placement
				(string-append "channel_refeval_placement_" (string-append (number->string cell) "_1"))
				"channel_refinement"
				(string-append "channel_refeval_" (string-append (number->string cell) "_1")))
			(sdedr:define-refinement-placement
				(string-append "channel_refeval_placement_" (string-append (number->string cell) "_2"))
				"channel_refinement"
				(string-append "channel_refeval_" (string-append (number->string cell) "_2")))
		)
	)

	(sde:build-mesh "snmesh" "-a -c boxmethod" "n@node@")
)


; ##############################      MAIN      ##############################

(build_device NF GR)
(refine_and_mesh NF GR)
