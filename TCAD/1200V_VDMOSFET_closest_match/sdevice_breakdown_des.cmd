# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "init_vdmos_des.cmd"

Solve
{
	* Initial Solution
	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
		Electron
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
		Electron
		Hole
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
		Electron
		Hole
		Temperature
	}

	Quasistationary
	(
		InitialStep = 2e-3
		MinStep = 2e-6
		MaxStep = 0.025
		Increment = 1.4
		Goal
		{
			Name = "drain_contact"
			Voltage = @Vd_nom@
		}
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
			* Temperature
		}
	}

	Set("drain_contact" mode current)
	QuasiStationary
	(
		InitialStep = 1e-6
		MinStep = 1e-9
		Maxstep = 0.05
		Increment = 1.4
		Goal
		{
			Name = "drain_contact"
			Current = @Id_max@
		}
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
			* Temperature
		}
	}
}
