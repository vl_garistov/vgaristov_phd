# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Initial settings for NMOS power device simulations

File
{
	********** INPUT FILES **********
	* Geometry, contacts doping and mesh
	Grid = "@tdr@"
	* Physical parameters
	Parameter = "@parameter@"
	********** OUTPUT FILES **********
	* Distributed variables
	Plot = "@tdrdat@"
	* Electrical characteristics at the electrodes
	Current = "@plot@"
	* Log file
	Output = "@log@"
}

Electrode
{
	{
		Name = "drain_contact"
		#if "@tool_label@" == "sdevice_output"
		Voltage = 0.05
		#else
		Voltage = 0.0
		#endif
	}
	{
		Name = "source_contact"
		Voltage = 0.0
	}
	{
		Name = "gate_contact"
		Voltage = 0.0
	}
}

Thermode
{
	{
		Name = "drain_contact"
		Temperature = 300
		SurfaceResistance = 0.0015
	}
	{
		Name = "source_contact"
		Temperature = 300
		SurfaceResistance = 0.005
	}
}

Physics
{
	Fermi
	AreaFactor = @AF@
	Temperature = @Tamb@
	DefaultParametersFromFile
	EffectiveIntrinsicDensity
	(
		OldSlotboom
		NoFermi
	)
	Recombination
	(
		SRH
		(
			DopingDependence
			(
				Nakagawa
			)
			TempDependence
			ElectricField
			(
				Lifetime = Hurkx
				DensityCorrection = Local
			)
		)
		SurfaceSRH
		Auger
		Avalanche
		(
			Hatakeyama
			Eparallel
		)
	)
	Mobility
	(
		ConstantMobility
		PhuMob
		Enormal
		(
			IALMob
			InterfaceCharge
		)
		HighFieldSaturation
		(
			CaugheyThomas
		)
	)
	IncompleteIonization
	(
		Split
		(
			Doping = "NitrogenConcentration"
			Weights = (0.5 0.5)
		)
	)
	Aniso
	(
		Mobility
		Avalanche
		Poisson
		Temperature
		direction(SimulationSystem) = (1, 0, 0)
	)
	Thermodynamic
	TEPower
	(
		Analytic
	)
	HeatCapacity
	(
		TempDep
	)
	ThermalConductivity
	(
		TempDep
		Resistivity
	)
}

## Interface charges at SiliconCarbide-Oxide interface
Physics(MaterialInterface = "4HSiC/SiO2")
{
	Traps
	(
		FixedCharge Conc = @FC@
	)
}

Math
{
	Digits = 7
	* Extrapolate
	ErrEff(electron) = 1e8
	ErrEff(hole) = 1e8
	Notdamped = 20
	Iterations = 18
	CheckRhsAfterUpdate
	RhsMin = 1e-4
	RHSMax = 1e60
	RHSFactor = 1e60
	ExitOnFailure
	* BreakAtOBA
	* BreakCriteria{Current (Contact="drain_contact" absval=1.0)}
	ExtendedPrecision(80)
	CDensityMin = 1e-25
	TensorGridAniso(aniso)
	NumberofThreads = 4
	Method = Blocked
	Transient = BE
	SubMethod = ILS
	(
		set = 12
	)
	ILSrc = "
		set (12)
		{
			iterative(gmres(150), tolrel = 1e-9, tolunprec = 1e-4, tolabs = 0, maxit = 200);
			preconditioning(ilut(1e-8, -1), left);
			ordering(symmetric=nd, nonsymmetric = mpsilst);
			options(compact = yes, linscale = 0, refineresidual = 5, verbose = 0);
		};"
}

Plot
{
	Potential SpaceCharge
	eDensity hDensity
	eCurrent hCurrent TotalCurrent/vector CurrentPotential
	ElectricField/vector
	eQuasiFermi hQuasiFermi
	egradQuasiFermi hgradQuasiFermi
	SRH Auger
	AvalancheGeneration eAvalanche hAvalanche
	eMobility hMobility
	eMobilityAniso hMobilityAniso
	eMobilityAnisoFactor hMobilityAnisoFactor
	DielectricConstant DielectricConstantAniso
	Doping DonorConcentration DonorPlusConcentration AcceptorConcentration AccepMinusConcentration
	NitrogenConcentration NitrogenActiveConcentration
	NitrogenConcentration_split1 NitrogenActiveConcentration_split1 NitrogenPlusConcentration_split1
	NitrogenConcentration_split2 NitrogenActiveConcentration_split2 NitrogenPlusConcentration_split2
	AluminumConcentration AluminumActiveConcentration
	ConductionBandEnergy ValenceBandEnergy BandGap
	
	* Traps visualization
	eTrappedCharge hTrappedCharge
	eInterfaceTrappedCharge hInterfaceTrappedCharge
	eGapStatesRecombination hGapStatesRecombination
	TotalInterfaceTrapConcentration

	LatticeTemperature
}
