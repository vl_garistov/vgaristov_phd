# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import svisualpylib.extract as ext

xlab = "Vd-@Vd@"
N = @node@
i = @node:index@
plot_name = "Plot_IdVg"
plot_filename = "../@previous@/IdVg_n@previous@_des.plt"
dataset_name = "PLT_IdVg({0})".format(N)
curve_name = "IdVg({})".format(N)
colors = ["green", "blue", "red", "orange", "magenta", "darkMagenta", "chocolate"]
num_colors = len(colors)
curve_color = colors[i % num_colors]

sv.load_file(filename = plot_filename, name = dataset_name)

sv.echo("Extracting parameters from Id-Vg  curve")

Vgs = sv.get_variable_data(dataset = dataset_name, varname = "gate_contact OuterVoltage")
Ids = sv.get_variable_data(dataset = dataset_name, varname = "drain_contact TotalCurrent")
Io = 1.0e-3
Vti = ext.extract_vti(name = "Vth", v_values = Vgs, i_values = Ids, i_o = Io)
gm = ext.extract_gm(name = "gm", v_values = Vgs, i_values = Ids)

sv.echo("Plotting Id-Vg curve")

if not sv.list_plots(plot_name):
	sv.create_plot(name = plot_name, xy = True)
sv.select_plots([plot_name])
sv.set_window_size("900x700")
sv.set_window_full(on = True)
sv.set_plot_prop(
	plot = plot_name,
	title = "IdVg",
	title_font_family = "arial",
	title_font_size = 12,
	title_font_color = "#000000",
	title_font_att = "bold")
sv.create_curve(
	name = curve_name,
	dataset = [dataset_name],
	plot = plot_name,
	axisX = "gate_contact OuterVoltage",
	axisY = "drain_contact TotalCurrent")
sv.set_curve_prop(
	curve = [curve_name],
	label = xlab,
	color = curve_color,
	line_style = "solid",
	line_width = 3)
sv.set_axis_prop(
	plot = plot_name,
	axis = "x",
	title = "Vg, [V]",
	title_font_size = 18,
	scale_font_size = 16,
	title_font_att = "bold",
	manual_precision = True,
	scale_precision = 0,
	scale_format = "fixed")
sv.set_axis_prop(
	plot = plot_name,
	axis = "y",
	title = "Id, [A]",
	title_font_size = 18,
	scale_font_size = 16,
	title_font_att = "bold",
	manual_precision = True,
	scale_precision = 0,
	scale_format = "fixed")
sv.set_legend_prop(
	plot = plot_name,
	label_font_family = "arial",
	label_font_size = 12,
	label_font_color = "#000000",
	label_font_att = "bold")

