# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import svisualpylib.extract as ext

N = @node@
plot_filename = "../@previous@/n@previous@_des.plt"
dataset_name = "PLT_IdVd({0})".format(N)

sv.load_file(filename = plot_filename, name = dataset_name)

sv.echo("Extracting parameters from Id-Vd  curve")

Vds = sv.get_variable_data(dataset = dataset_name, varname = "drain_contact InnerVoltage")
Ids = sv.get_variable_data(dataset = dataset_name, varname = "drain_contact TotalCurrent")
Ibvi = 1e-4
ext.extract_bvi(name = "Vbr", v_values = Vds, i_values = Ids, i_o = Ibvi)
