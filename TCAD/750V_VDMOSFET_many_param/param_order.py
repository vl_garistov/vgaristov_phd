# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

param_order = [
	Param.Default,
	Param.Tox,
	Param.Ng,
	Param.Tg,
	Param.Npwb,
	Param.Npwt,
	Param.Tpwb,
	Param.Tpwt,
	Param.L,
	Param.Npp,
	Param.Tpp,
	Param.Lpp,
	Param.Lsg,
	Param.Nnp,
	Param.Tnp,
	Param.Lnp,
	Param.Njfet,
	Param.Tjfet,
	Param.Tjfet,
	Param.Ljfet,
	Param.Nepi,
	Param.Tepi,
	Param.Nsub,
	Param.Tsub,
	Param.FC,
	Param.FC,
	Param.GR,
	Param.NF,
	Param.NF,
	Param.Tamb
]
