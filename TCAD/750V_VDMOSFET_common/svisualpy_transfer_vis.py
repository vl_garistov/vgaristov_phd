# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# If you change the value of this macro, make sure to also change the next setdep line accordingly
#define EXPERIMENTS_PER_PLOT	5
#if @node:index@ > EXPERIMENTS_PER_PLOT
#setdep @node:first:+4@
#endif
#if (@node:index@ - 1) % EXPERIMENTS_PER_PLOT != 0
# Creates a dependency on the node above this one
#setdep @node:-1@
#endif

import svisualpylib.extract as ext
#include "colors.py"
#include "param_types.py"
#include "param_order.py"

def extract_variables():
	plot_filename = "../@previous@/IdVg_n@previous@_des.plt"
	dataset_name = "PLT_IdVg(@node@)"

	sv.echo("Extracting parameters from Id-Vg curve")

	sv.load_file(filename = plot_filename, name = dataset_name)
	Vgs_unsanitized = sv.get_variable_data(dataset = dataset_name, varname = "gate_contact OuterVoltage")
	Ids_unsanitized = sv.get_variable_data(dataset = dataset_name, varname = "drain_contact TotalCurrent")
	Ids, Vgs = ext.remove_zeros(x_values = Ids_unsanitized, y_values = Vgs_unsanitized, iplists = "x")
	Io = 1.0e-3
	Vt_i = ext.extract_vti(name = "Vt_i", v_values = Vgs, i_values = Ids, i_o = Io)
	Vt_gm = ext.extract_vtgm(name = "Vt_gm", v_values = Vgs, i_values = Ids)
	Vt_sat = ext.extract_vtsat(name = "Vt_sat", v_values = Vgs, i_values = Ids)
	gm = ext.extract_gm(name = "gm", v_values = Vgs, i_values = Ids)
	Vo = 1.0e-1
	Ioff = ext.extract_ioff(name = "Ioff", v_values = Vgs, i_values = Ids, v_o = Vo)
	Vgo = @Vgo@
	SS = ext.extract_ss(name = "SS", v_values = Vgs, i_values = Ids, v_o = Vgo)

def plot_curves():
	N = @node@
	N_top = @node:first@
	N_sim = @previous@
	N_top_sim = @node:first|-1@
	node_offset = N - N_sim
	plot_name = "output_plot"
	window_size = "900x700"
	num_Vds_values = EXPERIMENTS_PER_PLOT

	# Only plot graphs for every num_Vds_values-th row, otherwise just record Vds
	if (N - N_top + 1) % num_Vds_values != 0:
		Vds_file = open("Vds_val.txt", "w")
		Vds_file.write("@Vd@")
		Vds_file.close()
		return

	sv.echo("Plotting curve to file")

	# The top row is the default case in which no parameter is varied
	if N == N_top + num_Vds_values - 1:
		# Draw the characteristic for the default case
		plot_title = "Transfer characteristic"

		create_and_conf_plot(plot_name, plot_title, window_size)

		sim_node = N_sim - num_Vds_values + 1
		i = 0
		while sim_node < N_sim:
			Vds_file = open("../{0}/Vds_val.txt".format(sim_node + node_offset), "r")
			Vds = Vds_file.readline(20).strip()
			Vds_file.close()
			curve_name = "Id_curve_{0}".format(i)
			curve_label = "Vds = " + Vds + " V"
			curve_color = colors_list[i]
			plot_filename = "../{0}/IdVg_n{0}_des.plt".format(sim_node)
			dataset_name = "PLT_IdVg({0})".format(sim_node + node_offset)
			sim_node += 1
			i += 1

			sv.load_file(filename = plot_filename, name = dataset_name)
			sv.create_curve(
				name = curve_name,
				dataset = [dataset_name],
				plot = plot_name,
				axisX = "gate_contact OuterVoltage",
				axisY = "drain_contact TotalCurrent")
			sv.set_curve_prop(
				curve = [curve_name],
				label = curve_label,
				color = curve_color,
				line_style = "solid",
				line_width = 3)

		# Already loaded during extract_variables()
		dataset_name = "PLT_IdVg(@node@)"
		curve_name = "Id_curve_{0}".format(i)
		curve_label = "Vds = @Vd@ V"
		curve_color = colors_list[i]
		sv.create_curve(
			name = curve_name,
			dataset = [dataset_name],
			plot = plot_name,
			axisX = "gate_contact OuterVoltage",
			axisY = "drain_contact TotalCurrent")
		sv.set_curve_prop(
			curve = [curve_name],
			label = curve_label,
			color = curve_color,
			line_style = "solid",
			line_width = 3)

		sv.export_view(
			filename = "../../../images/transfer.png",
			format = "PNG",
			overwrite = True,
			plots = [plot_name],
			resolution = window_size)

		# The other nodes will need both Vds and parameter values of the default case
		Vds_file = open("Vds_val.txt", "w")
		Vds_file.write("@Vd@")
		Vds_file.close()
		store_param_values()

	else:
		param_index = ((N - N_top + 1) // num_Vds_values) - 1
		param = param_order[param_index]
		short_name = get_param_short_name(param)
		long_name = get_param_long_name(param)
		value = get_param_value(param)
		unit = get_param_unit(param)

		param_file = open("../" + str(N_top + num_Vds_values - 1) + "/" + short_name + "_default_val.txt", "r")
		default_value = param_file.readline(20).strip()
		param_file.close()

		plot_title = "Transfer characteristic, variable " + long_name

		create_and_conf_plot(plot_name, plot_title, window_size)

		# Draw this node's curves
		sim_node = N_sim - num_Vds_values + 1
		i = 0
		while sim_node < N_sim:
			Vds_file = open("../{0}/Vds_val.txt".format(sim_node + node_offset), "r")
			Vds = Vds_file.readline(20).strip()
			Vds_file.close()
			curve_name = "Id_curve_{0}".format(i)
			curve_label = short_name + " = " + value + " " + unit + ", Vds = " + Vds + " V"
			curve_color = colors_list[i]
			plot_filename = "../{0}/IdVg_n{0}_des.plt".format(sim_node)
			dataset_name = "PLT_IdVg({0})".format(sim_node + node_offset)
			sim_node += 1
			i += 1

			sv.load_file(filename = plot_filename, name = dataset_name)
			sv.create_curve(
				name = curve_name,
				dataset = [dataset_name],
				plot = plot_name,
				axisX = "gate_contact OuterVoltage",
				axisY = "drain_contact TotalCurrent")
			sv.set_curve_prop(
				curve = [curve_name],
				label = curve_label,
				color = curve_color,
				line_style = "solid",
				line_width = 3)

		# Already loaded during extract_variables()
		dataset_name = "PLT_IdVg(@node@)"
		curve_name = "Id_curve_{0}".format(i)
		curve_label = short_name + " = " + value + " " + unit + "Vds = @Vd@ V"
		curve_color = colors_list[i]
		sv.create_curve(
			name = curve_name,
			dataset = [dataset_name],
			plot = plot_name,
			axisX = "gate_contact OuterVoltage",
			axisY = "drain_contact TotalCurrent")
		sv.set_curve_prop(
			curve = [curve_name],
			label = curve_label,
			color = curve_color,
			line_style = "solid",
			line_width = 3)

		# Draw the curves for the default case
		sim_node = N_top_sim
		i = 0
		while sim_node <= N_top_sim + num_Vds_values - 1:
			Vds_file = open("../{0}/Vds_val.txt".format(sim_node + node_offset), "r")
			Vds = Vds_file.readline(20).strip()
			Vds_file.close()
			curve_name = "Id_curve_def_{0}".format(i)
			curve_label = short_name + " = " + default_value + " " + unit + ", Vds = " + Vds + " V"
			curve_color = colors_list[i]
			plot_filename = "../{0}/IdVg_n{0}_des.plt".format(sim_node)
			dataset_name = "PLT_IdVg({0})".format(sim_node + node_offset)
			sim_node += 1
			i += 1

			sv.load_file(filename = plot_filename, name = dataset_name)
			sv.create_curve(
				name = curve_name,
				dataset = [dataset_name],
				plot = plot_name,
				axisX = "gate_contact OuterVoltage",
				axisY = "drain_contact TotalCurrent")
			sv.set_curve_prop(
				curve = [curve_name],
				label = curve_label,
				color = curve_color,
				line_style = "dash",
				line_width = 3)

		sv.export_view(
			filename = "../../../images/transfer_" + short_name + ".png",
			format = "PNG",
			overwrite = True,
			plots = [plot_name],
			resolution = window_size)

def store_param_values():
	for param in param_order:
		short_name = get_param_short_name(param)
		long_name = get_param_long_name(param)
		value = get_param_value(param)
		unit = get_param_unit(param)

		param_file = open(short_name + "_default_val.txt", "w")
		param_file.write(value)
		param_file.close()

def create_and_conf_plot(plot_name, plot_title, window_size):
	x_title = "Vgs, [V]"
	y_title = "Id, [A]"

	sv.create_plot(name = plot_name, xy = True)
	sv.select_plots([plot_name])
	sv.set_window_size(window_size)
	sv.set_window_full(on = True)
	sv.set_plot_prop(
		plot = plot_name,
		title = plot_title,
		title_font_family = "arial",
		title_font_size = 18,
		title_font_color = "#000000",
		title_font_att = "bold",
		hide_grid = False)
	sv.set_axis_prop(
		plot = plot_name,
		axis = "x",
		title = x_title,
		title_font_size = 18,
		scale_font_size = 16,
		title_font_att = "bold")
	sv.set_axis_prop(
		plot = plot_name,
		axis = "y",
		title = y_title,
		title_font_size = 18,
		scale_font_size = 16,
		title_font_att = "bold")
	sv.set_legend_prop(
		plot = plot_name,
		label_font_family = "arial",
		label_font_size = 12,
		label_font_color = "#000000",
		label_font_att = "bold")

if __name__ == "__main__":
	extract_variables()
	plot_curves()
