# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
sys.path.insert(0, './')
from param_order import *

class Result(Enum):
	Vt_i = 0
	Vt_gm = 1
	Vt_sat = 2
	gm = 3
	SS = 4
	Ron = 5
	Vbr = 6
	Vtlp = 7
	Ioff = 8
	SCWT = 9
	Qg = 10
	Vf = 11
	Rd_diff = 12
	trr = 13

def get_result_from_short_name(short_name):
	match short_name:
		case "Vt_i":
			return Result.Vt_i
		case "Vt_gm":
			return Result.Vt_gm
		case "Vt_sat":
			return Result.Vt_sat
		case "gm":
			return Result.gm
		case "SS":
			return Result.SS
		case "Ron":
			return Result.Ron
		case "Vbr":
			return Result.Vbr
		case "Vtlp":
			return Result.Vtlp
		case "Ioff":
			return Result.Ioff
		case "SCWT":
			return Result.SCWT
		case "Qg":
			return Result.Qg
		case "Vf":
			return Result.Vf
		case "Rd_diff":
			return Result.Rd_diff
		case "trr":
			return Result.trr

class ParamRecord:
	def __init__(self, timestamp = 0, type = None, node = 0, value = None):
		self.timestamp = timestamp
		self.type = type
		self.node = node
		self.value = value

def read_param_record(line):
	record_parts = line.split(' ')
	if len(record_parts) != 4:
		return None
	return ParamRecord(
		timestamp = int(record_parts[0]),
		type = get_param_from_short_name(record_parts[2]),
		node = int(record_parts[1]),
		value = record_parts[3])

class ResultRecord:
	def __init__(self, timestamp = 0, type = None, node = 0, value = None):
		self.timestamp = timestamp
		self.type = type
		self.node = node
		self.value = value

def read_result_record(line):
	record_parts = line.split(' ')
	if len(record_parts) != 4:
		return None
	return ResultRecord(
		timestamp = int(record_parts[0]),
		type = get_result_from_short_name(record_parts[2]),
		node = int(record_parts[1]),
		value = record_parts[3])

class Experiment:
	def __init__(self):
		# For every experiment there is a value of every parameter and every result
		self.params = [None] * len(Param)
		self.results = [None] * len(Result)

if __name__ == "__main__":
	# Read the values of the parameters in pvars.dat
	param_records = []
	with open('pvars.dat', 'r') as fp:
		for line in fp:
			new_record = read_param_record(line)
			if new_record is not None:
				param_records.append(new_record)

	# Check if anything was read successfuly from pvars.dat
	if len(param_records) == 0:
		exit(-1)

	# Find the node ID of the topmost parameter dump node
	top_param_node = param_records[0].node
	for record in param_records:
		if record.node < top_param_node:
			top_param_node = record.node

	# Assemble the parameter values in a sorted array
	experiments = [None] * len(param_order)
	for record in param_records:
		if experiments[record.node - top_param_node] is None:
			experiments[record.node - top_param_node] = Experiment()
			experiments[record.node - top_param_node].params[record.type.value] = record
		elif experiments[record.node - top_param_node].params[record.type.value] is None:
			experiments[record.node - top_param_node].params[record.type.value] = record
		elif experiments[record.node - top_param_node].params[record.type.value].timestamp < record.timestamp:
			experiments[record.node - top_param_node].params[record.type.value] = record

	# Check if all experiment objects have been initialized
	for experiment in experiments:
		if experiment is None:
			exit(-3);

	# Read the values of the results in gvars.dat
	result_records = [] * len(Result)
	with open('gvars.dat', 'r') as fp:
		for line in fp:
			new_record = read_result_record(line)
			if new_record is not None:
				result_records[record.type.value].append(new_record)

	# Check if all results were read successfuly from gvars.dat
	for result_group in result_records:
		if len(result_group) == 0:
			exit(-2)

	# Find the node IDs of the topmost result dump nodes
	top_result_nodes = [None] * len(Result)
	for result_group in result_records:
		result_type = result_group[0].type
		top_result_nodes[result_type.value] = result_group[0].node
		for record in result_group:
			if record.node < top_result_nodes[result_type.value]:
				top_result_nodes[result_type.value] = record.node

	# Assemble the result values in a sorted array
	for result_group in result_records:
		result_type = result_group[0].type
		for record in result_group:
			if experiments[record.node - top_result_nodes[result_type.value]].results[record.type.value] is None:
				experiments[record.node - top_result_nodes[result_type.value]].results[record.type.value] = record
			elif experiments[record.node - top_result_nodes[result_type.value]].results[record.type.value].timestamp < record.timestamp:
				experiments[record.node - top_result_nodes[result_type.value]].results[record.type.value] = record
