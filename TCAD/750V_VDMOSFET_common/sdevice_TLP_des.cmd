# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Based on TLP simulation in example project BCD.

#define T_TLP   100e-9
#define T_TLP_r 10e-9
#define T_TLP_f 10e-9
#define T_TLP_e 15e-9

#define T1_TLP T_TLP_r
#define T2_TLP @<T_TLP_r + T_TLP>@
#define T3_TLP @<T_TLP_r + T_TLP + T_TLP_f>@

#define NUM_INTERVALS 10

#include "init_vdmos_des.cmd"
#include "math_vdmos_des.cmd"

Solve
{
	* Initial Solution
	Coupled
	(
		Iterations = 10000
		LineSearchDamping = 1e-3
	)
	{
		Poisson
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-3
	)
	{
		Poisson
		Electron
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-3
	)
	{
		Poisson
		Electron
		Hole
	}

	*Coupled
	*(
	*	Iterations = 1000
	*	LineSearchDamping = 1e-3
	*)
	*{
	*	Poisson
	*	Electron
	*	Hole
	*	Temperature
	*}

	NewCurrentPrefix = "TLP_"
	Transient
	(
		InitialTime = 0.0
		FinalTime = @<T_TLP_r + T_TLP + T_TLP_f + T_TLP_e>@
		InitialStep = 1e-14
		MinStep = 1e-20
		MaxStep = 0.1
		Increment = 1.45
		Decrement = 3.0
		Plot
		{
			Range = (0, T_TLP_r)
			Intervals = NUM_INTERVALS
		}
		Plot
		{
			Range = (T_TLP_r, @<T_TLP_r + T_TLP>@)
			Intervals = NUM_INTERVALS
		}
		Plot
		{
			Range = (@<T_TLP_r + T_TLP>@, @<T_TLP_r + T_TLP + T_TLP_f>@)
			Intervals = NUM_INTERVALS
		}
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
			*Temperature
		}
	}
}