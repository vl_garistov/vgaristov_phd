# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define VDS_MAX 1e4

#include "init_vdmos_des.cmd"
#include "math_vdmos_des.cmd"

Solve
{
	* Initial Solution
	Coupled
	(
		Iterations = 10000
		LineSearchDamping = 1e-3
	)
	{
		Poisson
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-3
	)
	{
		Poisson
		Electron
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-3
	)
	{
		Poisson
		Electron
		Hole
	}

	NewCurrentPrefix = "BV_"
	Transient
	(
		InitialTime = 0.0
		FinalTime = @t_end@
		InitialStep = 1e-9
		MinStep = 1e-20
		MaxStep = 0.01
		Increment = 2.0
		Decrement = 3.0
		Goal
		{
			Name = "drain_contact"
			Voltage = VDS_MAX
		}
		BreakCriteria
		{
			Current
			(
				contact = "drain_contact"
				AbsVal = @Id_max@
			)
		}
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
		}
		Plot
		(
			Time =
			(
				Range = (0 @t_end@)
				Intervals = 40
			)
			NoOverwrite
		)
	}
}
