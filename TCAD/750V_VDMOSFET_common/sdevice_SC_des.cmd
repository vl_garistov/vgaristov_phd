# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define T_SC	10e-6
#define T_SC_r	10e-9
#define T_SC_f	10e-9
#define T_SC_e	980e-9

#if "@GR@" == "0"
#define T_SC_bs	0.0
#else
#define T_SC_bs	1.0
#endif

#define T1_SC @<T_SC_bs + T_SC_r>@
#define T2_SC @<T_SC_bs + T_SC_r + T_SC>@
#define T3_SC @<T_SC_bs + T_SC_r + T_SC + T_SC_f>@

#define VG_SC	20
#define T_CRIT	900

#include "init_vdmos_des.cmd"
#include "math_vdmos_des.cmd"

Solve
{
	* Initial Solution
	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
		Electron
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
		Electron
		Hole
	}

	* Vd setup
	#if "@GR@" == "0"
	Quasistationary
	(
		InitialStep = 1e-4
		MinStep = 1e-8
		MaxStep = 1.0
		Increment = 1.5
		Decrement = 3.0
		Goal
		{
			Name = "drain_contact"
			Voltage = @Vd_SC@
		}
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
		}
	}
	#else
	Transient
	(
		InitialTime = 0.0
		FinalTime = T_SC_bs
		InitialStep = 1e-12
		MinStep = 1e-21
		MaxStep = 1e-3
		Increment = 2.0
		Decrement = 3.0
		Goal
		{
			Name = "drain_contact"
			Voltage = @Vd_SC@
		}
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
		}
	}
	#endif

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
		Electron
		Hole
		Temperature
	}

	* Vg sweep
	NewCurrentPrefix="SC_"
	Transient
	(
		InitialTime = T_SC_bs
		FinalTime = @<T_SC_bs + T_SC + T_SC_r + T_SC_f + T_SC_e>@
		InitialStep = 1e-12
		MinStep = 1e-21
		MaxStep = 5e-8
		Increment = 2.0
		Decrement = 3.0
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
			Temperature
		}
		Plot
		(
			Time =
			(
				Range = (T_SC_bs @<T_SC_bs + T_SC + T_SC_r + T_SC_f + T_SC_e>@)
				Intervals = 10
			)
			NoOverwrite
		)
	}
}
