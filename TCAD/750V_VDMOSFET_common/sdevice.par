# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define ParFileDir parameters

Material="4HSiC" {
  #includeext "ParFileDir/4HSiC.par"
}
Material="Aluminum" {
  #includeext "ParFileDir/Aluminum.par"
}
Material="Default" {
  # WARNING: no parameter file found for material Default in the material database
}
Material="GatePolySilicon" {
  # WARNING: no parameter file found for material GatePolySilicon in the material database
}
Material="Insulator1" {
  #includetext "ParFileDir/Insulator1.par"
}
Material="Nickel" {
  # WARNING: no parameter file found for material Nickel in the material database
  # Use Al Kappa values for Ni and Ti because they are not defined
  #includeext "ParFileDir/Nickel_custom.par"
}
Material="NickelSilicide" {
  # WARNING: no parameter file found for material NickelSilicide in the material database
}
Material="Si3N4" {
  #includeext "ParFileDir/Si3N4.par"
}
Material="SiO2" {
  #includeext "ParFileDir/SiO2.par"
}
Material="Silicon" {
  #includeext "ParFileDir/Silicon.par"
}
Material="Silver" {
  #includeext "ParFileDir/Silver.par"
}
Material="Titanium" {
  #includeext "ParFileDir/Titanium.par"
  # Use Al Kappa values for Ni and Ti because they are not defined
  #includeext "ParFileDir/Titanium_custom.par"
}
Material="Vacuum" {
  #includeext "ParFileDir/Vacuum.par"
}
