# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#setdep @node:-1@

import time

#include "param_order.py"

if __name__ == "__main__":
	param_file = open('../../../pvars.dat', 'a')
	param_iter = iter(Param)
	next(param_iter)
	for param in param_iter:
		print("{} @node@ {} {}".format(int(time.time()), get_param_short_name(param), get_param_value(param)), file = param_file)
	param_file.close()
