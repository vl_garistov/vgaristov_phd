# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Creates dependencies on the first and previous node in this column
#setdep @node:-1@
#setdep @node:first@

import svisualpylib.extract as ext
#include "colors.py"
#include "param_order.py"

def extract_variables():
	plot_filename = "../@previous@/TLP_n@previous@_des.plt"
	dataset_name = "PLT_VdTLP(@node@)"
	sv.load_file(filename = plot_filename, name = dataset_name)
	sv.echo("Extracting parameters from Vd-t curve")
	Vds = sv.get_variable_data(dataset = dataset_name, varname = "drain_contact InnerVoltage")
	t = sv.get_variable_data(dataset = dataset_name, varname = "time")
	# Some weird shit happens at the end of the pulse so we ignore the last 20% of values
	ext.extract_extremum(name = "Vtlp", y_values = Vds[:int(len(Vds) * 0.8)], extremum_type = "max")

def plot_curves():
	N = @node@
	N_top = @node:first@
	N_top_sim = @node:first|-1@
	plot_name = "Vtlp_plot"
	window_size = "900x700"
	# Already loaded during extract_variables()
	dataset_name = "PLT_VdTLP(@node@)"

	sv.echo("Plotting curve to file")

	# The top row is the default case in which no parameter is varied
	if N == N_top:
		# Draw the characteristic for the default case
		plot_title = "TLP characteristic"
		curve_name = "Vds_curve"
		curve_label = "Vds"
		curve_color = "red"

		create_and_conf_plot(plot_name, plot_title, window_size)
		sv.create_curve(
			name = curve_name,
			dataset = [dataset_name],
			plot = plot_name,
			axisX = "time",
			axisY = "drain_contact InnerVoltage")
		sv.set_curve_prop(
			curve = [curve_name],
			label = curve_label,
			color = curve_color,
			line_style = "solid",
			line_width = 3)
		sv.export_view(
			filename = "../../../images/Vtlp.png",
			format = "PNG",
			overwrite = True,
			plots = [plot_name],
			resolution = window_size)
		store_param_values()

	else:
		param_index = N - N_top
		param = param_order[param_index]
		short_name = get_param_short_name(param)
		long_name = get_param_long_name(param)
		value = get_param_value(param)
		unit = get_param_unit(param)

		# If there is more than one node that varies this parameter and this is not
		# the last of them, then simply record the parameter value and move on.
		if param_index < (len(param_order) - 1) and param == param_order[param_index + 1]:
			param_file = open(short_name + "_val.txt", "w")
			param_file.write(value)
			param_file.close()
		else:
			default_dataset_name = "PLT_VdTLP(@node:first@)"
			default_plot_filename = "../@previous:first@/TLP_n@previous:first@_des.plt"
			sv.load_file(
				filename = default_plot_filename,
				name = default_dataset_name)

			param_file = open("../@node:first@/" + short_name + "_default_val.txt", "r")
			default_value = param_file.readline(20).strip()
			param_file.close()

			plot_title = "TLP characteristic, variable " + long_name
			curve_name = "Vds_curve_0"
			curve_label = short_name + " = " + value + " " + unit
			curve_color = "red"

			create_and_conf_plot(plot_name, plot_title, window_size)

			# Draw this node's curve
			sv.create_curve(
				name = curve_name,
				dataset = [dataset_name],
				plot = plot_name,
				axisX = "time",
				axisY = "drain_contact InnerVoltage")
			sv.set_curve_prop(
				curve = [curve_name],
				label = curve_label,
				color = curve_color,
				line_style = "solid",
				line_width = 3)

			param_index -= 1
			curve_index = 1
			# Draw the curves for the previous nodes that vary the same parameter
			while param_index > 0 and param_order[param_index] == param:
				next_node_id = N_top + param_index
				next_sim_node_id = N_top_sim + param_index
				param_file = open("../" + str(next_node_id) + "/" + short_name + "_val.txt", "r")
				next_value = param_file.readline(20).strip()
				param_file.close()

				next_dataset_name = "PLT_VdTLP({0})".format(next_node_id)
				next_plot_filename = "../{0}/TLP_n{0}_des.plt".format(next_sim_node_id)
				sv.load_file(
					filename = next_plot_filename,
					name = next_dataset_name)

				next_curve_name = "Vds_curve_" + str(curve_index)
				next_curve_label = short_name + " = " + next_value + " " + unit
				next_curve_color = colors_list[curve_index % len(colors_list)]

				sv.create_curve(
					name = next_curve_name,
					dataset = [next_dataset_name],
					plot = plot_name,
					axisX = "time",
					axisY = "drain_contact InnerVoltage")
				sv.set_curve_prop(
					curve = [next_curve_name],
					label = next_curve_label,
					color = next_curve_color,
					line_style = "solid",
					line_width = 3)

				param_index -= 1
				curve_index += 1

			# Draw the curve for the default case
			default_curve_name = "Vds_curve_default"
			default_curve_label = short_name + " = " + default_value + " " + unit
			default_curve_color = "red"

			sv.create_curve(
				name = default_curve_name,
				dataset = [default_dataset_name],
				plot = plot_name,
				axisX = "time",
				axisY = "drain_contact InnerVoltage")
			sv.set_curve_prop(
				curve = [default_curve_name],
				label = default_curve_label,
				color = default_curve_color,
				line_style = "solid",
				line_width = 3)

			sv.export_view(
				filename = "../../../images/Vtlp_" + short_name + ".png",
				format = "PNG",
				overwrite = True,
				plots = [plot_name],
				resolution = window_size)

def store_param_values():
	for param in param_order:
		short_name = get_param_short_name(param)
		long_name = get_param_long_name(param)
		value = get_param_value(param)
		unit = get_param_unit(param)

		param_file = open(short_name + "_default_val.txt", "w")
		param_file.write(value)
		param_file.close()

def create_and_conf_plot(plot_name, plot_title, window_size):
	x_title = "time, [s]"
	y_title = "Vds, [V]"

	sv.create_plot(name = plot_name, xy = True)
	sv.select_plots([plot_name])
	sv.set_window_size(window_size)
	sv.set_window_full(on = True)
	sv.set_plot_prop(
		plot = plot_name,
		title = plot_title,
		title_font_family = "arial",
		title_font_size = 18,
		title_font_color = "#000000",
		title_font_att = "bold",
		hide_grid = False,
		show_legend = False)
	sv.set_axis_prop(
		plot = plot_name,
		axis = "x",
		title = x_title,
		title_font_size = 18,
		scale_font_size = 16,
		title_font_att = "bold")
	sv.set_axis_prop(
		plot = plot_name,
		axis = "y",
		title = y_title,
		title_font_size = 18,
		scale_font_size = 16,
		title_font_att = "bold")
	sv.set_legend_prop(
		plot = plot_name,
		label_font_family = "arial",
		label_font_size = 12,
		label_font_color = "#000000",
		label_font_att = "bold")

if __name__ == "__main__":
	extract_variables()
	plot_curves()
