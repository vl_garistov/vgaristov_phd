# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "math_vdmos_des.cmd"

#define T_SW_D1     1e-6
#define T_SW_R      10e-09
#define T_SW_F      10e-09
#define T_SW_FIRST  @<Ld_sw * Id_sw / Vd_sw>@

File
{
	* Log file
	Output = "@log@"
}

Device NMOS
{
	#include "init_vdmos_des.cmd"
}

System
{
	NMOS MOS1
	(
		source_contact = 2
		gate_contact = 2
		drain_contact = 3
	)
	{
		File
		{
			Current = "n@node@_mos1_des.plt"
		}
	}
	NMOS MOS2
	(
		source_contact = 0
		gate_contact = 1
		drain_contact = 2
	)
	{
		File
		{
			Current = "n@node@_mos2_des.plt"
		}
	}

	Inductor_pset Ll (2 3) {inductance = @Ld_sw@}
	Resistor_pset Rg (11 1) {resistance = @Rg_sw@}

	Vsource_pset Vdd (3 0) {dc = 0}
	Vsource_pset Vg (11 0)
	{
		pwl =
		(
			0                                                               0.0
			@<T_SW_D1>@                                                     0.0
			@<T_SW_D1 + T_SW_R>@                                            @Vg_sw@
			@<T_SW_D1 + T_SW_R + T_SW_FIRST>@                               @Vg_sw@
			@<T_SW_D1 + T_SW_R + T_SW_FIRST + T_SW_F>@                      0.0
			@<T_SW_D1 + T_SW_R + (1.5 * T_SW_FIRST) + T_SW_F>@              0.0
			@<T_SW_D1 + (2 * T_SW_R) + (1.5 * T_SW_FIRST) + T_SW_F>@        @Vg_sw@
			@<T_SW_D1 + (2 * T_SW_R) + (2.0 * T_SW_FIRST) + T_SW_F>@        @Vg_sw@
			@<T_SW_D1 + (2 * T_SW_R) + (2.0 * T_SW_FIRST) + (2 * T_SW_F)>@  0.0
			@<T_SW_D1 + (2 * T_SW_R) + (4.0 * T_SW_FIRST) + (2 * T_SW_F)>@  0.0
		)
	}

	Plot "n@node@_sys_des.plt"
	(
		time() v(11) v(2) i(MOS1, 4) i(MOS1, 3) i(MOS2, 2) i(MOS2, 1) i(Ll, 3) i(Ll, 4)
	)
}

Solve
{
	Coupled
	(
		Iterations = 1000
	)
	{
		Poisson
	}

	Coupled
	(
		Iterations = 1000
	)
	{
		Poisson
		Contact
		Circuit
	}

	Coupled
	(
		Iterations = 1000
	)
	{
		Poisson
		Electron
		Contact
		Circuit
	}

	Coupled
	(
		Iterations = 1000
	)
	{
		Poisson
		Electron
		Hole
		Contact
		Circuit
	}

	Quasistationary
	(
		InitialStep = 1e-6
		MinStep = 1e-10
		MaxStep = 1.0
		Increment = 1.2
		Decrement = 1.5
		Goal
		{
			Parameter = Vdd.dc
			Voltage = @Vd_sw@
		}
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
			Contact
			Circuit
		}
	}

	NewCurrentPrefix="SW_"
	Transient
	(
		InitialTime = 0
		FinalTime = @<T_SW_D1 + (2 * T_SW_R) + (4.0 * T_SW_FIRST) + (2 * T_SW_F)>@
		InitialStep = 1e-9
		MinStep = 1e-15
		MaxStep = @<T_SW_FIRST / 10.0>@
		Increment = 1.2
		Decrement = 2.0
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
			Contact
			Circuit
		}
	}
}

