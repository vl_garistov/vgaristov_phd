# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Why does this file exist?
# Because apparently the current version of Sentaurus is bugged and per-device Math
# sections in mixed-mode simulations cause errors. Go figure!

Math
{
	NumberofThreads = 4

	Extrapolate
	(
		* Prevents convergence issues caused by incorrect extrapolation with very low carrier concentrations
		LowDensityLimit = 1e8
		NumberOfFailures = 2
	)
	Iterations = 20
	Notdamped = 30

	ExtendedPrecision(80)
	Digits = 7
	CheckTransientError
	TransientDigits = 5

	CheckRhsAfterUpdate
	RelErrControl
	RhsMin = 1e-7
	RHSMax = 1e60
	RHSFactor = 1e60
	* Typos?
	* ErrEffSlivovaRakia(electron) = 1e8
	* ErrEff(hole) = 1e8
	ErrRef(electron) = 1e8
	ErrRef(hole) = 1e8

	ExitOnFailure
	* BreakAtOBA
	* AvalPostProcessing
	*BreakCriteria
	*{
	*	LatticeTemperature
	*	(
	*		* SiC melting point
	*		MaxVal = 3100
	*	)
	*}

	CDensityMin = 1e-25
	AvalDensGradQF
	TensorGridAniso(aniso)

	* The default is actually more accurate but slower and less convergent
	#if "@tool_label@" == "sdevice_Qg"
	* Transient = BE
	#endif

	Method = Blocked
	SubMethod = ILS
	(
		set = 12
	)
	ILSrc = "
		set (12)
		{
			iterative(gmres(150), tolrel = 1e-9, tolunprec = 1e-4, tolabs = 0, maxit = 400);
			preconditioning(ilut(1e-8, -1), left);
			ordering(symmetric=nd, nonsymmetric = mpsilst);
			options(compact = yes, linscale = 0, refineresidual = 5, verbose = 0);
		};"
}

#if "@tool_label@" == "sdevice_SC"
Math (material = "Aluminum")
{
	BreakCriteria
	{
		LatticeTemperature
		(
			MaxVal = T_CRIT
		)
	}
}
#endif
