# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "init_vdmos_des.cmd"
#include "math_vdmos_des.cmd"

Solve
{
	* Initial Solution
	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
		Electron
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
		Electron
		Hole
	}

	*Coupled
	*(
	*	Iterations = 1000
	*	LineSearchDamping = 1e-2
	*)
	*{
	*	Poisson
	*	Electron
	*	Hole
	*	Temperature
	*}

	* Vd setup
	#if "@GR@" == "0"
	Quasistationary
	(
		InitialStep = 1e-4
		MinStep = 1e-8
		MaxStep = 1.0
		Increment = 1.5
		Decrement = 3.0
		Goal
		{
			Name = "drain_contact"
			Voltage = @Vd@
		}
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
			*Temperature
		}
	}
	#else
	Transient
	(
		InitialTime = 0.0
		FinalTime = 1.0
		InitialStep = 1e-4
		MinStep = 1e-9
		MaxStep = 1e-2
		Increment = 2.0
		Decrement = 3.0
		Goal
		{
			Name = "drain_contact"
			Voltage = @Vd@
		}
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
			*Temperature
		}
	}
	#endif

	* Vg sweep
	NewCurrentPrefix="IdVg_"
	Quasistationary
	(
		InitialStep = 1e-4
		MinStep = 1e-8
		MaxStep = 0.01
		Increment = 1.5
		Decrement = 3.0
		Goal
		{
			Name = "gate_contact"
			Voltage = @Vg_max@
		}
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
			*Temperature
		}
	}
}
