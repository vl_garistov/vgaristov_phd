# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Creates dependencies on the first and previous node in this column
#setdep @node:-1@
#setdep @node:first@

import svisualpylib.extract as ext
#include "colors.py"
#include "param_order.py"

def extract_variables():
	plot_filename = "../@previous@/SC_n@previous@_des.plt"
	dataset_name = "PLT_SC(@node@)"
	sv.load_file(filename = plot_filename, name = dataset_name)
	sv.echo("Extracting parameters from short-circuit simulation")
	t = sv.get_variable_data(dataset = dataset_name, varname = "time")
	sv.echo("DOE: SCWT", str(t[-1]))

def plot_curves():
	N = @node@
	N_top = @node:first@
	N_top_sim = @node:first|-1@
	plot_name = "SC_plot"
	window_size = "900x700"
	# Already loaded during extract_variables()
	dataset_name = "PLT_SC(@node@)"

	sv.echo("Plotting curve to file")

	# The top row is the default case in which no parameter is varied
	if N == N_top:
		# Draw the characteristic for the default case
		plot_title = "Short-circuit pulse"
		Id_curve_name = "Id_curve"
		Id_curve_label = "Id"
		Id_curve_color = "red"
		T_curve_name = "T_curve"
		T_curve_label = "Tmax"
		T_curve_color = "red"

		create_and_conf_plot(plot_name, plot_title, window_size)
		sv.create_curve(
			name = Id_curve_name,
			dataset = [dataset_name],
			plot = plot_name,
			axisX = "time",
			axisY = "drain_contact TotalCurrent")
		sv.set_curve_prop(
			curve = [Id_curve_name],
			label = Id_curve_label,
			color = Id_curve_color,
			line_style = "solid",
			line_width = 3)
		sv.create_curve(
			name = T_curve_name,
			dataset = [dataset_name],
			plot = plot_name,
			axisX = "time",
			axisY = "Tmax")
		sv.set_curve_prop(
			curve = [T_curve_name],
			axis = "right",
			label = T_curve_label,
			color = T_curve_color,
			line_style = "dash",
			line_width = 3)
		sv.export_view(
			filename = "../../../images/SC.png",
			format = "PNG",
			overwrite = True,
			plots = [plot_name],
			resolution = window_size)
		store_param_values()

	else:
		param_index = N - N_top
		param = param_order[param_index]
		short_name = get_param_short_name(param)
		long_name = get_param_long_name(param)
		value = get_param_value(param)
		unit = get_param_unit(param)

		# If there is more than one node that varies this parameter and this is not
		# the last of them, then simply record the parameter value and move on.
		if param_index < (len(param_order) - 1) and param == param_order[param_index + 1]:
			param_file = open(short_name + "_val.txt", "w")
			param_file.write(value)
			param_file.close()
		else:
			default_dataset_name = "PLT_SC(@node:first@)"
			default_plot_filename = "../@previous:first@/SC_n@previous:first@_des.plt"
			sv.load_file(
				filename = default_plot_filename,
				name = default_dataset_name)

			param_file = open("../@node:first@/" + short_name + "_default_val.txt", "r")
			default_value = param_file.readline(20).strip()
			param_file.close()

			plot_title = "Short-circuit pulse, variable " + long_name
			Id_curve_name = "Id_curve_0"
			Id_curve_label = "Id, " + short_name + " = " + value + " " + unit
			Id_curve_color = "red"
			T_curve_name = "T_curve_0"
			T_curve_label = "Tmax, " + short_name + " = " + value + " " + unit
			T_curve_color = "red"

			create_and_conf_plot(plot_name, plot_title, window_size)

			# Draw this node's curves
			sv.create_curve(
				name = Id_curve_name,
				dataset = [dataset_name],
				plot = plot_name,
				axisX = "time",
				axisY = "drain_contact TotalCurrent")
			sv.set_curve_prop(
				curve = [Id_curve_name],
				label = Id_curve_label,
				color = Id_curve_color,
				line_style = "solid",
				line_width = 3)
			sv.create_curve(
				name = T_curve_name,
				dataset = [dataset_name],
				plot = plot_name,
				axisX = "time",
				axisY = "Tmax")
			sv.set_curve_prop(
				curve = [T_curve_name],
				axis = "right",
				label = T_curve_label,
				color = T_curve_color,
				line_style = "dash",
				line_width = 3)

			param_index -= 1
			curve_index = 1
			# Draw the curves for the previous nodes that vary the same parameter
			while param_index > 0 and param_order[param_index] == param:
				next_node_id = N_top + param_index
				next_sim_node_id = N_top_sim + param_index
				param_file = open("../" + str(next_node_id) + "/" + short_name + "_val.txt", "r")
				next_value = param_file.readline(20).strip()
				param_file.close()

				next_dataset_name = "PLT_SC({0})".format(next_node_id)
				next_plot_filename = "../{0}/SC_n{0}_des.plt".format(next_sim_node_id)
				sv.load_file(
					filename = next_plot_filename,
					name = next_dataset_name)

				next_Id_curve_name = "Id_curve_" + str(curve_index)
				next_Id_curve_label = "Id, " + short_name + " = " + next_value + " " + unit
				next_Id_curve_color = colors_list[curve_index % len(colors_list)]
				next_T_curve_name = "T_curve_" + str(curve_index)
				next_T_curve_label = "Tmax, " + short_name + " = " + next_value + " " + unit
				next_T_curve_color = colors_list[curve_index % len(colors_list)]

				sv.create_curve(
					name = next_Id_curve_name,
					dataset = [next_dataset_name],
					plot = plot_name,
					axisX = "time",
					axisY = "drain_contact TotalCurrent")
				sv.set_curve_prop(
					curve = [next_Id_curve_name],
					label = next_Id_curve_label,
					color = next_Id_curve_color,
					line_style = "solid",
					line_width = 3)
				sv.create_curve(
					name = next_T_curve_name,
					dataset = [next_dataset_name],
					plot = plot_name,
					axisX = "time",
					axisY = "Tmax")
				sv.set_curve_prop(
					curve = [next_T_curve_name],
					axis = "right",
					label = next_T_curve_label,
					color = next_T_curve_color,
					line_style = "dash",
					line_width = 3)

				param_index -= 1
				curve_index += 1

			# Draw the curves for the default case
			default_Id_curve_name = "Id_curve_default"
			default_Id_curve_label = "Id, " + short_name + " = " + default_value + " " + unit
			default_Id_curve_color = next_curve_color = colors_list[curve_index % len(colors_list)]
			default_T_curve_name = "T_curve_default"
			default_T_curve_label = "Tmax, " + short_name + " = " + default_value + " " + unit
			default_T_curve_color = next_curve_color = colors_list[curve_index % len(colors_list)]

			sv.create_curve(
				name = default_Id_curve_name,
				dataset = [default_dataset_name],
				plot = plot_name,
				axisX = "time",
				axisY = "drain_contact TotalCurrent")
			sv.set_curve_prop(
				curve = [default_Id_curve_name],
				label = default_Id_curve_label,
				color = default_Id_curve_color,
				line_style = "solid",
				line_width = 3)
			sv.create_curve(
				name = default_T_curve_name,
				dataset = [default_dataset_name],
				plot = plot_name,
				axisX = "time",
				axisY = "drain_contact TotalCurrent")
			sv.set_curve_prop(
				curve = [default_T_curve_name],
				axis = "right",
				label = default_T_curve_label,
				color = default_T_curve_color,
				line_style = "dash",
				line_width = 3)

			sv.export_view(
				filename = "../../../images/SC_" + short_name + ".png",
				format = "PNG",
				overwrite = True,
				plots = [plot_name],
				resolution = window_size)

def store_param_values():
	for param in param_order:
		short_name = get_param_short_name(param)
		long_name = get_param_long_name(param)
		value = get_param_value(param)
		unit = get_param_unit(param)

		param_file = open(short_name + "_default_val.txt", "w")
		param_file.write(value)
		param_file.close()

def create_and_conf_plot(plot_name, plot_title, window_size):
	x_title = "time, [s]"
	y_title = "Id, [A]"
	y2_title = "Tmax, [K]"

	sv.create_plot(name = plot_name, xy = True)
	sv.select_plots([plot_name])
	sv.set_window_size(window_size)
	sv.set_window_full(on = True)
	sv.set_plot_prop(
		plot = plot_name,
		title = plot_title,
		title_font_family = "arial",
		title_font_size = 18,
		title_font_color = "#000000",
		title_font_att = "bold",
		hide_grid = False)
	sv.set_axis_prop(
		plot = plot_name,
		axis = "x",
		title = x_title,
		title_font_size = 18,
		scale_font_size = 16,
		title_font_att = "bold")
	sv.set_axis_prop(
		plot = plot_name,
		axis = "y",
		title = y_title,
		title_font_size = 18,
		scale_font_size = 16,
		title_font_att = "bold")
	sv.set_axis_prop(
		plot = plot_name,
		axis = "y2",
		title = y2_title,
		title_font_size = 18,
		scale_font_size = 16,
		title_font_att = "bold")
	sv.set_legend_prop(
		plot = plot_name,
		location = "bottom_right",
		label_font_family = "arial",
		label_font_size = 12,
		label_font_color = "#000000",
		label_font_att = "bold")

if __name__ == "__main__":
	extract_variables()
	plot_curves()
