# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Initial settings for NMOS power device simulations

File
{
	********** INPUT FILES **********
	* Geometry, contacts doping and mesh
	Grid = "@tdr@"
	* Physical parameters
	Parameter = "@parameter@"
	********** OUTPUT FILES **********
	* Distributed variables
	Plot = "@tdrdat@"
	* Electrical characteristics at the electrodes
	Current = "@plot@"
}

Electrode
{
	{
		Name = "drain_contact"
		Voltage = 0.0
		#if "@tool_label@" == "sdevice_breakdown"
		Resist = @<Rcs * AF>@
		#elif "@tool_label@" == "sdevice_TLP"
		Current =
		(
			(0, 0.0e-16)
			(T1_TLP, @<Itlp / AF>@)
			(T2_TLP, @<Itlp / AF>@)
			(T3_TLP, 0.0)
		)
		#else
		Resist = @<1e-3 * AF>@
		#endif
	}
	{
		Name = "source_contact"
		Voltage = 0.0
		Resist = @<1e-3 * AF>@
	}
	{
		Name = "gate_contact"
		#if "@tool_label@" == "sdevice_SC"
		Voltage =
		(
			(0, 0.0)
			(T1_SC, VG_SC)
			(T2_SC, VG_SC)
			(T3_SC, 0.0)
		)
		#else
		Voltage = 0.0
		#endif
		Resist = @<1e-3 * AF>@
	}
	#if @GR@
	#{
	#	Name = regexp("guard_rings_contact_[0-9]+")
	#	Voltage = 0.0
	#	Resist = @<1e8*AF>@
	#}
	#{
	#	Name = regexp("channel_stop_contact_[0-9]+")
	#	Voltage = 0.0
	#	Resist = @<1e8*AF>@
	#}
	#endif
}

Thermode
{
	{
		Name = "drain_contact"
		Temperature = @Tamb@
		SurfaceResistance = 0.0015
	}
	{
		Name = "source_contact"
		Temperature = @Tamb@
		SurfaceResistance = 0.005
	}
}

Physics
{
	Fermi
	AreaFactor = @AF@
	Temperature = @Tamb@
	DefaultParametersFromFile
	EffectiveIntrinsicDensity
	(
		OldSlotboom
		NoFermi
	)
	Recombination
	(
		SRH
		(
			DopingDependence
			(
				Nakagawa
			)
			TempDependence
			ElectricField
			(
				Lifetime = Hurkx
				DensityCorrection = Local
			)
		)
		SurfaceSRH
		Auger
		Avalanche
		(
			Hatakeyama
			Eparallel
		)
	)
	Mobility
	(
		ConstantMobility
		PhuMob
		Enormal
		(
			IALMob
			InterfaceCharge
		)
		HighFieldSaturation
		(
			CaugheyThomas
		)
	)
	IncompleteIonization
	(
		Split
		(
			Doping = "NitrogenConcentration"
			Weights = (0.5 0.5)
		)
	)
	Aniso
	(
		Mobility
		Avalanche
		Poisson
		Temperature
		direction(SimulationSystem) = (1, 0, 0)
	)
	Thermodynamic
	TEPower
	(
		Analytic
	)
	HeatCapacity
	(
		TempDep
	)
	ThermalConductivity
	(
		TempDep
		Resistivity
	)
}

## Interface charges at SiliconCarbide-Oxide interface
Physics(MaterialInterface = "4HSiC/SiO2")
{
	Traps
	(
		FixedCharge Conc = @FC@
	)
}

Plot
{
	Potential SpaceCharge
	eDensity hDensity
	eCurrent hCurrent TotalCurrent/vector CurrentPotential
	ElectricField/vector
	eQuasiFermi hQuasiFermi
	egradQuasiFermi hgradQuasiFermi
	SRH Auger
	AvalancheGeneration eAvalanche hAvalanche
	eMobility hMobility
	eMobilityAniso hMobilityAniso
	eMobilityAnisoFactor hMobilityAnisoFactor
	DielectricConstant DielectricConstantAniso
	Doping DonorConcentration DonorPlusConcentration AcceptorConcentration AccepMinusConcentration
	NitrogenConcentration NitrogenActiveConcentration
	NitrogenConcentration_split1 NitrogenActiveConcentration_split1 NitrogenPlusConcentration_split1
	NitrogenConcentration_split2 NitrogenActiveConcentration_split2 NitrogenPlusConcentration_split2
	AluminumConcentration AluminumActiveConcentration
	ConductionBandEnergy ValenceBandEnergy BandGap

	* Traps visualization
	eTrappedCharge hTrappedCharge
	eInterfaceTrappedCharge hInterfaceTrappedCharge
	eGapStatesRecombination hGapStatesRecombination
	TotalInterfaceTrapConcentration

	LatticeTemperature
}
