* Copyright (c) 1994-2018 Synopsys, Inc.
* This parameter file and the associated documentation are proprietary
* to Synopsys, Inc.  This parameter file may only be used in accordance
* with the terms and conditions of a written license agreement with
* Synopsys, Inc.  All other use, reproduction, or distribution of this
* parameter file is strictly prohibited.


Resistivity
{ * Resist(T) = Resist0 * ( 1 + TempCoef * ( T - 273 ) )
	Resist0	= 5.0000e-05	# [ohm*cm]
	TempCoef	= 3.8000e-03	# [1/K]
}

* Reference: SOPRA N&K Database
ComplexRefractiveIndex {
  TableInterpolation = PositiveSpline, PositiveSpline 
  Formula = 1
  NumericalTable(
    0.2066  1.16  1.21;
    0.2084  1.1675  1.21625;
    0.2101  1.18  1.22;
    0.2119  1.195 1.22125;
    0.2138  1.21  1.22;
    0.2156  1.22125 1.215;
    0.2175  1.23  1.21;
    0.2194  1.23625 1.210625;
    0.2214  1.24  1.21;
    0.2234  1.244375  1.2025;
    0.2254  1.24  1.19;
    0.2275  1.199375  1.165;
    0.2296  1.18  1.15;
    0.2317  1.266875  1.178125;
    0.2339  1.35  1.21;
    0.2362  1.31625 1.20875;
    0.2384  1.26  1.2;
    0.2407  1.25  1.19875;
    0.2431  1.25  1.2;
    0.2455  1.245625  1.204375;
    0.2480  1.24  1.21;
    0.2505  1.23  1.215;
    0.2530  1.22  1.22;
    0.2556  1.215625  1.224375;
    0.2583  1.21  1.23;
    0.2610  1.195 1.23875;
    0.2638  1.18  1.25;
    0.2666  1.174375  1.264375;
    0.2695  1.17  1.28;
    0.2725  1.160625  1.295625;
    0.2755  1.15  1.31;
    0.2786  1.140625  1.319375;
    0.2818  1.13  1.33;
    0.2850  1.116875  1.346875;
    0.2883  1.1 1.37;
    0.2917  1.07375 1.401875;
    0.2952  1.05  1.44;
    0.2988  1.041875  1.484375;
    0.3024  1.04  1.53;
    0.3061  1.038125  1.568125;
    0.3100  1.04  1.61;
    0.3139  1.046875  1.66875;
    0.3179  1.06  1.73;
    0.3220  1.0825  1.783125;
    0.3263  1.11  1.83;
    0.3306  1.13875 1.8675;
    0.3351  1.17  1.9;
    0.3397  1.205 1.93125;
    0.3444  1.24  1.96;
    0.3493  1.27  1.985625;
    0.3542  1.3 2.01;
    0.3594  1.334375  2.03625;
    0.3647  1.37  2.06;
    0.3701  1.405625  2.07625;
    0.3757  1.44  2.09;
    0.3815  1.47125 2.105;
    0.3875  1.5 2.12;
    0.3936  1.52625 2.135625;
    0.3999  1.55  2.15;
    0.4065  1.570625  2.159375;
    0.4133  1.59  2.17;
    0.4203  1.609375  2.18875;
    0.4275  1.63  2.21;
    0.4350  1.655625  2.23;
    0.4428  1.68  2.25;
    0.4509  1.695625  2.269375;
    0.4592  1.71  2.29;
    0.4679  1.73  2.314375;
    0.4769  1.75  2.34;
    0.4862  1.765625  2.363125;
    0.4959	1.78	2.39;
    0.5061  1.79375 2.4275;
    0.5166  1.81  2.47;
    0.5276  1.833125  2.513125;
    0.5391  1.86  2.56;
    0.5510  1.8875  2.614375;
    0.5636  1.92  2.67;
    0.5767  1.9625  2.72;
    0.5904  2.01  2.77;
    0.6048  2.05875 2.824375;
    0.6199  2.11  2.88;
    0.6358  2.1625  2.934375;
    0.6525  2.22  2.99;
    0.6702  2.285625  3.049375;
    0.6888  2.36  3.11;
    0.7085  2.44625 3.173125;
    0.7293  2.54  3.23;
    0.7514  2.63625 3.27125;
    0.7749  2.74  3.3;
    0.7999  2.860625  3.316875;
    0.8266  2.98  3.32;
    0.8551  3.083125  3.303125;
    0.8856  3.17  3.28;
    0.9184  3.2325  3.259375;
    0.9537  3.28  3.25;
    0.9919  3.314375  3.266875;
    1.033 3.35  3.3;
    1.078 3.405 3.345625;
    1.127 3.47  3.4;
    1.181 3.5425  3.459375;
    1.240 3.62  3.52;
  )
}

