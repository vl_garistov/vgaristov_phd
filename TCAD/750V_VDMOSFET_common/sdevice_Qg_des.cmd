# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "math_vdmos_des.cmd"

File
{
	* Log file
	Output = "@log@"
}

Device NMOS
{
	#include "init_vdmos_des.cmd"
}

System
{
	NMOS MOS1
	(
		source_contact = 0
		gate_contact = 2
		drain_contact = 3
	)
	{
		File
		{
			Current = "n@node@_des.plt"
		}
	}

	Diode_pset d1 (3 4)
	Isource_pset i1 (3 4) {dc = 0.0}
	Isource_pset ig (0 1) {pwl = (0 0 1e-6 5e-4 2e-6 5e-4)}
	Vsource_pset vd (5 0) {dc = 0.0}

	Resistor_pset ri1 (3 4) {resistance = 1e8}
	Resistor_pset rig (0 1) {resistance = 1e8}
	Resistor_pset rg (1 2) {resistance = 1e-3}
	Resistor_pset rv (4 5) {resistance = 1e-3}

	## Initialize nodes 1, 2, 3 and 5 to 0 V
	## They are automatically unset at the start of a Transient simulation and when overridden
	Initialize
	(
		1 = 0
		2 = 0
		3 = 0
		5 = 0
	)

	Plot "n@node@_sys_des.plt" (time() v(1) v(2) v(3) v(4) i(MOS1, 3))
}

Solve
{
	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
		Contact
		Circuit
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
		Electron
		Contact
		Circuit
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
		Electron
		Hole
		Contact
		Circuit
	}

	Unset (3)
	Unset (5)
	Quasistationary
	(
		InitialStep = 5e-4
		MinStep = 1e-8
		MaxStep = 1.0
		Increment = 2.5
		Decrement = 3.0
		Goal
		{
			Parameter = vd.dc
			Value = 30.0
		}
	)
	{
		Coupled
		(
			Iterations = 30
		)
		{
			Poisson
			Electron
			Hole
			Contact
			Circuit
		}
	}
	Quasistationary
	(
		InitialStep = 1e-4
		MinStep = 1e-8
		MaxStep = 1.0
		Increment = 2.0
		Decrement = 3.0
		Goal
		{
			Parameter = i1.dc
			Value = -10.0
		}
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
			Contact
			Circuit
		}
	}

	NewCurrent = "QG_"
	Unset (1)
	Unset (2)
	Transient
	(
		InitialTime = 0
		FinalTime = 5e-4
		InitialStep = 1e-12
		MinStep = 1e-15
		MaxStep = 1e-5
		BreakCriteria
		{
			Voltage
			(
				Contact = "gate_contact"
				AbsVal = @Vg_q@
			)
		}
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
			Contact
			Circuit
		}
	}
}
