# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "math_vdmos_des.cmd"

File
{
	* Log file
	Output = "@log@"
}

Device NMOS
{
	#include "init_vdmos_des.cmd"
}

System
{
	NMOS MOS1
	(
		source_contact = 0
		gate_contact = 1
		drain_contact = 2
	)
	{
		File
		{
			Current = "n@node@_SW_des.plt"
		}
	}
	NMOS MOS2
	(
		source_contact = 2
		gate_contact = 2
		drain_contact = 3
	)
	{
		File
		{
			Current = "n@node@_DUT_des.plt"
		}
	}

	Vsource_pset vd (3 0) {dc = 0.0}
	Isource_pset id (0 2) {dc = 0.0}
	Vsource_pset vsw (1 0) {pwl = (0 0.0 10e-9 20.0 20e-9 20.0)}

	Plot "n@node@_sys_des.plt" (time() v(1) v(2) i(MOS2, 2))
}

Solve
{
	Set (1 = 0)
	Set (2 = 0)
	Set (3 = 0)

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
		Contact
		Circuit
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
		Electron
		Contact
		Circuit
	}

	Coupled
	(
		Iterations = 1000
		LineSearchDamping = 1e-2
	)
	{
		Poisson
		Electron
		Hole
		Contact
		Circuit
	}

	Unset (2)
	Quasistationary
	(
		InitialStep = 5e-10
		MinStep = 1e-14
		MaxStep = 1.0
		Increment = 2.5
		Decrement = 3.0
		Goal
		{
			Parameter = id.dc
			Value = @Itrr@
		}
	)
	{
		Coupled
		(
			Iterations = 30
		)
		{
			Poisson
			Electron
			Hole
			Contact
			Circuit
		}
	}

	Unset (3)
	Quasistationary
	(
		InitialStep = 5e-4
		MinStep = 1e-8
		MaxStep = 1.0
		Increment = 2.5
		Decrement = 3.0
		Goal
		{
			Parameter = vd.dc
			Value = @Vtrr@
		}
	)
	{
		Coupled
		(
			Iterations = 30
		)
		{
			Poisson
			Electron
			Hole
			Contact
			Circuit
		}
	}

	NewCurrent = "TRR_"
	Unset (1)
	Transient
	(
		InitialTime = 0
		FinalTime = 5e-6
		InitialStep = 1e-10
		MinStep = 1e-17
		MaxStep = 1e-6
		Increment = 2.5
		Decrement = 3.0
	)
	{
		Coupled
		{
			Poisson
			Electron
			Hole
			Contact
			Circuit
		}
	}
}
