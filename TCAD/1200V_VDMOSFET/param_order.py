# VGaristov_PhD
# Scripts, command files and parameters used in Vladimir Garistov's PhD thesis
# Copyright (C) 2024-2025 Vladimir Garistov <vl.garistov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from enum import Enum

class Param(Enum):
	Default = 0
	Tox = 1
	Ng = 2
	Tg = 3
	Npwb = 4
	Npwt = 5
	Tpwb = 6
	Tpwt = 7
	L = 8
	Npp = 9
	Tpp = 10
	Lpp = 11
	Lsg = 12
	Nnp = 13
	Tnp = 14
	Lnp = 15
	Njfet = 16
	Tjfet = 17
	Ljfet = 18
	Nepi = 19
	Tepi = 20
	Nsub = 21
	Tsub = 22
	FC = 23
	GR = 24
	NF = 25
	Tamb = 26

param_order = [
	Param.Default
]

def get_param_long_name(param):
	match param:
		case Param.Default:
			return "default"
		case Param.Tox:
			return "gate oxide thickness"
		case Param.Ng:
			return "gate doping"
		case Param.Tg:
			return "gate thickness"
		case Param.Npwb:
			return "burried P-well doping"
		case Param.Npwt:
			return "top P-well doping"
		case Param.Tpwb:
			return "burried P-well thickness"
		case Param.Tpwt:
			return "top P-well thickness"
		case Param.L:
			return "channel length"
		case Param.Npp:
			return "P+ doping"
		case Param.Tpp:
			return "P+ thickness"
		case Param.Lpp:
			return "P+ length"
		case Param.Lsg:
			return "source-gate overlap"
		case Param.Nnp:
			return "N+ doping"
		case Param.Tnp:
			return "N+ thickness"
		case Param.Lnp:
			return "N+ length"
		case Param.Njfet:
			return "JFET doping"
		case Param.Tjfet:
			return "JFET thickness"
		case Param.Ljfet:
			return "JFET length"
		case Param.Nepi:
			return "epitaxial doping"
		case Param.Tepi:
			return "epitaxial thickness"
		case Param.Nsub:
			return "substrate doping"
		case Param.Tsub:
			return "substrate thickness"
		case Param.FC:
			return "trapped charge density"
		case Param.GR:
			return "edge termination"
		case Param.NF:
			return "number of cells"
		case Param.Tamb:
			return "temperature"

def get_param_short_name(param):
	match param:
		case Param.Default:
			return "Def."
		case Param.Tox:
			return "Tox"
		case Param.Ng:
			return "Ng"
		case Param.Tg:
			return "Tg"
		case Param.Npwb:
			return "Npwb"
		case Param.Npwt:
			return "Npwt"
		case Param.Tpwb:
			return "Tpwb"
		case Param.Tpwt:
			return "Tpwt"
		case Param.L:
			return "L"
		case Param.Npp:
			return "Npp"
		case Param.Tpp:
			return "Tpp"
		case Param.Lpp:
			return "Lpp"
		case Param.Lsg:
			return "Lsg"
		case Param.Nnp:
			return "Nnp"
		case Param.Tnp:
			return "Tnp"
		case Param.Lnp:
			return "Lnp"
		case Param.Njfet:
			return "Njfet"
		case Param.Tjfet:
			return "Tjfet"
		case Param.Ljfet:
			return "Ljfet"
		case Param.Nepi:
			return "Nepi"
		case Param.Tepi:
			return "Tepi"
		case Param.Nsub:
			return "Nsub"
		case Param.Tsub:
			return "Tsub"
		case Param.FC:
			return "FC"
		case Param.GR:
			return "GR"
		case Param.NF:
			return "NF"
		case Param.Tamb:
			return "Tamb"

def get_param_from_short_name(short_name):
	match short_name:
		case "Def.":
			return Param.Default
		case "Tox":
			return Param.Tox
		case "Ng":
			return Param.Ng
		case "Tg":
			return Param.Tg
		case "Npwb":
			return Param.Npwb
		case "Npwt":
			return Param.Npwt
		case "Tpwb":
			return Param.Tpwb
		case "Tpwt":
			return Param.Tpwt
		case "L":
			return Param.L
		case "Npp":
			return Param.Npp
		case "Tpp":
			return Param.Tpp
		case "Lpp":
			return Param.Lpp
		case "Lsg":
			return Param.Lsg
		case "Nnp":
			return Param.Nnp
		case "Tnp":
			return Param.Tnp
		case "Lnp":
			return Param.Lnp
		case "Njfet":
			return Param.Njfet
		case "Tjfet":
			return Param.Tjfet
		case "Ljfet":
			return Param.Ljfet
		case "Nepi":
			return Param.Nepi
		case "Tepi":
			return Param.Tepi
		case "Nsub":
			return Param.Nsub
		case "Tsub":
			return Param.Tsub
		case "FC":
			return Param.FC
		case "GR":
			return Param.GR
		case "NF":
			return Param.NF
		case "Tamb":
			return Param.Tamb

def get_param_value(param):
	match param:
		case Param.Default:
			return "N/A"
		case Param.Tox:
			return "@gate_oxide_thickness@"
		case Param.Ng:
			return "@gate_dop@"
		case Param.Tg:
			return "@gate_thickness@"
		case Param.Npwb:
			return "@Pwell_bury_dop@"
		case Param.Npwt:
			return "@Pwell_top_dop@"
		case Param.Tpwb:
			return "@Pwell_bury_thickness@"
		case Param.Tpwt:
			return "@Pwell_top_thickness@"
		case Param.L:
			return "@L@"
		case Param.Npp:
			return "@Pp_dop@"
		case Param.Tpp:
			return "@Pp_thickness@"
		case Param.Lpp:
			return "@Pp_length@"
		case Param.Lsg:
			return "@source_gate_overlap@"
		case Param.Nnp:
			return "@Np_dop@"
		case Param.Tnp:
			return "@Np_thickness@"
		case Param.Lnp:
			return "@Np_length@"
		case Param.Njfet:
			return "@JFET_dop@"
		case Param.Tjfet:
			return "@JFET_thickness@"
		case Param.Ljfet:
			return "@JFET_length@"
		case Param.Nepi:
			return "@epi_dop@"
		case Param.Tepi:
			return "@epi_thickness@"
		case Param.Nsub:
			return "@sub_dop@"
		case Param.Tsub:
			return "@sub_thickness@"
		case Param.FC:
			return "@FC@"
		case Param.GR:
			return "@GR@"
		case Param.NF:
			return "@NF@"
		case Param.Tamb:
			return "@Tamb@"

def get_param_unit(param):
	match param:
		case Param.Default:
			return ""
		case Param.Tox:
			return "um"
		case Param.Ng:
			return "cm^-3"
		case Param.Tg:
			return "um"
		case Param.Npwb:
			return "cm^-3"
		case Param.Npwt:
			return "cm^-3"
		case Param.Tpwb:
			return "um"
		case Param.Tpwt:
			return "um"
		case Param.L:
			return "um"
		case Param.Npp:
			return "cm^-3"
		case Param.Tpp:
			return "um"
		case Param.Lpp:
			return "um"
		case Param.Lsg:
			return "um"
		case Param.Nnp:
			return "cm^-3"
		case Param.Tnp:
			return "um"
		case Param.Lnp:
			return "um"
		case Param.Njfet:
			return "cm^-3"
		case Param.Tjfet:
			return "um"
		case Param.Ljfet:
			return "um"
		case Param.Nepi:
			return "cm^-3"
		case Param.Tepi:
			return "um"
		case Param.Nsub:
			return "cm^-3"
		case Param.Tsub:
			return "um"
		case Param.FC:
			return "cm^-2"
		case Param.GR:
			return ""
		case Param.NF:
			return ""
		case Param.Tamb:
			return "K"
